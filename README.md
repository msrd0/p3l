# P³L Compiler

This is a compiler for P³L (probabilistic pointer programming language) that I design
as part of my bachelor thesis.

## Types

- `bool` boolean value
- `int` integer value (either 32 or 64 bit, depending on the target architecture)
- `()` empty tuple (also the default return value of functions, like `void` in C)
- `(..)` tuple
- `Rc<_>` reference-counting pointer
- `Option<_>` optional value, can be `None` or `Some(_)`
- `$ident`, `$ident<..>` custom type, must have previously been typedef'ed

## Syntax

```
$program   => $typedefs $functions $inputs $main $outputs

$typedefs  => $typedef $typedefs
            | ε

$functions => $function $functions
            | ε

$inputs    => $input $inputs
            | ε

$main      => $stmt $block

$outputs   => $output $outputs
            | ε

$typedef   => type $ident = $type;
            | type $ident<$identlist> = $type;

$type      => bool
            | int
            | ()
            | ($typelist)
            | ($typelist,)
            | Rc<$type>
            | Option<$type>
            | $ident
            | $ident<$typelist>

$typelist  => $type | $type, $typelist

$function  => fn $ident($params) { $block }
            | fn $ident($params) -> $type { $block }
            | fn $ident<$identlist>($params) { $block }
            | fn $ident<$identlist>($params) -> $type { $block }

$params    => $paramlist | ε

$paramlist => $ident: $type, $paramlist
            | $ident: $type

$input     => input!($ident);

$output    => output!($identlist);

$stmt      => let $ident: $type = $expr;
            | $ident = $expr;
            | $ifstmt
            | { $block } [$float] { $block }
            | while $ifcond { $block }
            | { $block }
            | return;
            | return $expr;
            | continue;
            | break;

$block     => $stmt $block
            | ε

$ifstmt    => if $ifcond { $block }
            | if $ifcond { $block } else $ifstmt
            | if $ifcond { $block } else { $block }

$ifcond    => $expr
            | let Some($ident) = $expr

$expr      => $expr $op $expr
            | !$expr
            | -$expr
            | *$expr
            | ($exprs)
            | ($exprlist,)
            | $expr.$index
            | Rc($expr)
            | Some($expr)
            | None
            | $int
            | $bool
            | $ident($exprs)
            | $ident<$typelist>($exprs)

$exprs     => $exprlist | ε

$exprlist  => $expr, $exprlist
            | $expr

$op        => + | - | * | /
            | == | != | < | <= | > | >=
            | && | ||

$ident     matches all valid Rust identifiers
$identlist => $ident | $ident, $identlist

$bool      => true | false
$index     matches all non-negative integer values
$int       matches all integer values
$float     matches all floating-point values
```
