use std::{env, fs::File, path::PathBuf};
use vergen::{vergen, Config, ShaKind};

fn main() -> anyhow::Result<()> {
	// provide version information
	let mut cfg = Config::default();
	let git = cfg.git_mut();
	*git.branch_mut() = false;
	*git.commit_count_mut() = true;
	*git.sha_kind_mut() = ShaKind::Short;
	vergen(cfg)?;

	// include the stdlib crate with the compiler
	let out_dir: PathBuf = env::var("OUT_DIR")?.parse()?;
	let archive_path = out_dir.join("stdlib.tar");
	println!("cargo:rustc-env=ARCHIVE_PATH={}", archive_path.display());
	let mut archive = tar::Builder::new(File::create(&archive_path)?);
	let stdlib_path = "stdlib";
	println!("cargo:rerun-if-changed={stdlib_path}");
	archive.append_dir_all(stdlib_path, stdlib_path)?;
	archive.finish()?;

	Ok(())
}
