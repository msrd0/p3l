var svg,xn,yn,tt,ttp,ttx,tty;
function e(c){return document.getElementById(c)}
function i(xn0,yn0){
	xn=xn0;yn=yn0;
	svg=document.getElementsByTagName("svg")[0];
	tt=e("tt");ttp=e("ttp");ttx=e("ttx");tty=e("tty");
	c()
}
function tr(x,y){tt.setAttribute('transform','translate('+x+','+y+')')}
function s(ev,p,x,y) {
	ttp.textContent=p+"%";
	ttx.textContent=xn+": "+x;
	tty.textContent=yn+": "+y;
	var r = ev.target.getBoundingClientRect();
	var p = ev.target.ownerSVGElement.createSVGPoint();
	p.x=r.x+r.width/2;
	p.y=r.top;
	p=p.matrixTransform(svg.getScreenCTM().inverse());
	tr(p.x,p.y-5);
}
function c(){tr(-999,-999)}