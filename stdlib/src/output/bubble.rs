use super::{OutputList, OutputPrinter, Svg};
use crate::int;
use indexmap::IndexMap;
use num_integer::div_ceil;
use std::{fmt::Write as _, io};
use svgwriter::{
	tags::{
		Circle, Group, Path, Rect, Script, Style, TSpan, TagWithCoreAttributes as _,
		TagWithGlobalEventAttributes as _, TagWithPresentationAttributes as _, Text
	},
	Data, Transform
};

/// Output printer for SVG Bar Charts. Works only with two
/// output variables.
pub struct BubbleChartPrinter {
	/// Set to true to enable SVG/XML pretty printing.
	pretty: bool
}

impl BubbleChartPrinter {
	pub fn new(pretty: bool) -> Self {
		Self { pretty }
	}
}

#[derive(Default)]
struct Axis {
	values: Vec<int>,
	min: Option<int>,
	max: Option<int>
}

impl Axis {
	fn add_value(&mut self, value: int) {
		self.values.push(value);
		if self.max.map(|max| max < value).unwrap_or(true) {
			self.max = Some(value);
		}
		if self.min.map(|min| min > value).unwrap_or(true) {
			self.min = Some(value);
		}
	}

	fn min(&self) -> int {
		self.min.unwrap_or(0)
	}

	fn max(&self) -> int {
		self.max.unwrap_or(0)
	}

	fn step(&self, preferred_step_count: usize) -> (int, usize) {
		let range = self.max() - self.min();
		if range == 0 {
			return (0, 1);
		}

		let mut step = 1;
		loop {
			let count = div_ceil(range, step) + 1;
			if (count - preferred_step_count as int).unsigned_abs() as usize
				<= preferred_step_count / 2
			{
				return (step, count as usize);
			}
			step += 1;
		}
	}
}

impl OutputPrinter for BubbleChartPrinter {
	fn print<I, E, W>(self, list: OutputList<I>, mut out: W) -> io::Result<()>
	where
		I: IntoIterator<Item = (E, usize)>,
		E: IntoIterator<Item = (&'static str, int)>,
		W: io::Write
	{
		writeln!(out, "<!--")?;
		writeln!(out, "Output Variable Analysis:")?;
		for (key, stats) in list.stats() {
			writeln!(out, "  {key}: {stats:?}")?;
		}
		writeln!(out, "-->")?;

		let mut percentages = Vec::new();
		let mut min_percentage: f32 = 100.0;
		let mut max_percentage: f32 = 0.0;
		let mut axis: IndexMap<&'static str, Axis> = IndexMap::new();
		for (entry, qty) in list.samples {
			let percentage = qty as f32 / list.n as f32 * 100.0;
			percentages.push(percentage);
			min_percentage = min_percentage.min(percentage);
			max_percentage = max_percentage.max(percentage);
			for (key, value) in entry {
				if !axis.contains_key(key) {
					axis.insert(key, Axis::default());
				}
				axis.get_mut(key).unwrap().add_value(value);
			}
		}

		let (yaxis_name, yaxis) = axis
			.swap_remove_index(1)
			.expect("Cannot use BubbleChartPrinter with less than two variables");
		let (xaxis_name, xaxis) = axis.swap_remove_index(0).unwrap();

		let chart_margin_x = 60;
		let chart_margin_y = 60;
		let bubble_min_r: f32 = 2.5;
		let bubble_max_r: f32 = 15.0;
		let axis_step_dist = 40;
		let (xaxis_step, xaxis_step_count) = xaxis.step(7);
		let (yaxis_step, yaxis_step_count) = yaxis.step(6);
		let chart_width = axis_step_dist * xaxis_step_count as i32;
		let chart_height = axis_step_dist * yaxis_step_count as i32;
		let svg_width = chart_width + 2 * chart_margin_x;
		let svg_height = chart_height + 2 * chart_margin_y;
		let mut svg = Svg::new(svg_width, svg_height)
			.with_onload(format!("i({xaxis_name:?},{yaxis_name:?})"));

		// add svg style and script
		let mut style = concat!(
			"*{font-family:\"Source Serif 4\",serif}",
			".ui,#tt{stroke:#000;stroke-width:1px;fill:#000;font-size:12pt}",
			"#tt{font-size:10pt}",
			"#tt path{fill:rgba(238,238,238,0.85)}",
			"#ttp{font-weight:bold}",
			"text,.bg,.b{stroke-width:0}",
			".bg{fill:#fff}",
			".b,#ttx,#tty{fill:#f60}",
			".b circle,circle.b{opacity:0.8}",
			// dark theme
			"@media(prefers-color-scheme: dark){",
			"#tt path{fill:rgba(34,34,34,0.7)}",
			".ui,#tt{stroke:#ccc;fill:#ccc}",
			".bg{fill:#222}",
			"}"
		)
		.to_owned();
		// allow svg to be printed to pdf by headless chromium
		write!(
			style,
			"@page{{margin:0;size:calc(20cm*{svg_width}/{svg_height}) 20cm}}"
		)
		.unwrap();
		svg.push(Style::new().append(style));
		svg.push(
			Script::new()
				.with_ty("text/ecmascript")
				.append(include_str!("bubble.js").trim())
		);

		// add svg background
		svg.push(
			Rect::new()
				.with_class("bg")
				.with_x(0)
				.with_y(0)
				.with_width(chart_width + 2 * chart_margin_x)
				.with_height(chart_height + 2 * chart_margin_y)
				.with_onmouseover("c()")
		);

		let mut group = Group::new()
			.with_transform(
				Transform::new().translate(chart_margin_x, chart_height + chart_margin_y)
			)
			.with_class("ui");

		// build bubbles
		let mut bubbles = Group::new().with_class("b").with_transform(
			Transform::new()
				.translate(axis_step_dist / 2, axis_step_dist / -2)
				.scale(1.0, -1.0)
		);
		for (i, percentage) in percentages.iter().enumerate() {
			let xvalue = xaxis.values[i];
			let yvalue = yaxis.values[i];

			let xpos =
				(xvalue - xaxis.min()) as f32 / xaxis_step as f32 * axis_step_dist as f32;
			let ypos =
				(yvalue - yaxis.min()) as f32 / yaxis_step as f32 * axis_step_dist as f32;
			// we want the area to grow proportional to the percentage, so the
			// radius grows proportional to the square root of the percentage
			let radius = bubble_min_r
				+ ((bubble_max_r - bubble_min_r)
					* ((percentage - min_percentage)
						/ (max_percentage - min_percentage))
						.sqrt());
			bubbles.push(
				Circle::new()
					.with_cx(xpos)
					.with_cy(ypos)
					.with_r(radius)
					.with_onmouseover(format!(
						"s(evt,\"{:.2}\",{},{})",
						percentage, xvalue, yvalue
					))
			);
		}
		group.push(bubbles);

		// build y axis line
		let mut axis_line0 = Data::new();
		let mut axis_line1 = Data::new();
		axis_line0.move_to(0, -chart_height).vert_line_to(0);
		axis_line1
			.move_to(chart_width, -chart_height)
			.vert_line_to(0);
		for i in 0 .. yaxis_step_count {
			let (offset, step) = match i {
				0 => (-3, -axis_step_dist / 2),
				_ => (-6, -axis_step_dist)
			};
			axis_line0.move_by(offset, step).horiz_line_by(6);
			axis_line1.move_by(offset, step).horiz_line_by(6);
		}

		// build x axis line
		axis_line0.move_to(chart_width, 0).horiz_line_to(0);
		axis_line1
			.move_to(chart_width, -chart_height)
			.horiz_line_to(0);
		for i in 0 .. xaxis_step_count {
			let (offset, step) = match i {
				0 => (-3, axis_step_dist / 2),
				_ => (-6, axis_step_dist)
			};
			axis_line0.move_by(step, offset).vert_line_by(6);
			axis_line1.move_by(step, offset).vert_line_by(6);
		}
		group.push(
			Group::new()
				.with_stroke_linejoin("arcs")
				.append(Path::new().with_d(axis_line0))
				.append(Path::new().with_d(axis_line1))
		);

		// build y axis descriptions
		let mut yaxis_text0 = Text::new()
			.with_text_anchor("end")
			.with_dominant_baseline("middle");
		let mut yaxis_text1 = Text::new().with_dominant_baseline("middle");
		for i in 0 .. yaxis_step_count {
			let text = format!("{}", yaxis.min() + i as int * yaxis_step,);
			yaxis_text0.push(
				TSpan::new()
					.with_x(-5)
					.with_y(i as i32 * -axis_step_dist - axis_step_dist / 2)
					.append(text.clone())
			);
			yaxis_text1.push(
				TSpan::new()
					.with_x(chart_width + 5)
					.with_y(i as i32 * -axis_step_dist - axis_step_dist / 2)
					.append(text)
			);
		}
		group.push(yaxis_text0);
		group.push(yaxis_text1);

		// build x axis descriptions
		let mut xaxis_text0 = Text::new().with_text_anchor("middle");
		let mut xaxis_text1 = Text::new()
			.with_text_anchor("middle")
			.with_dominant_baseline("hanging");
		for i in 0 .. xaxis_step_count {
			let text = format!("{}", xaxis.min() + i as int * xaxis_step,);
			xaxis_text0.push(
				TSpan::new()
					.with_x(i as i32 * axis_step_dist + axis_step_dist / 2)
					.with_y(-chart_height - 5)
					.append(text.clone())
			);
			xaxis_text1.push(
				TSpan::new()
					.with_x(i as i32 * axis_step_dist + axis_step_dist / 2)
					.with_y(5)
					.append(text)
			);
		}
		group.push(xaxis_text0);
		group.push(xaxis_text1);

		// build y axis legend
		group.push(
			Group::new()
				.with_transform(
					Transform::new()
						.translate(15 - chart_margin_x, chart_height / -2)
						.rotate(270.0)
				)
				.append(
					Text::new()
						.with_text_anchor("middle")
						.with_dominant_baseline("middle")
						.append(yaxis_name)
				)
		);
		group.push(
			Group::new()
				.with_transform(
					Transform::new()
						.translate(chart_width + chart_margin_x - 15, chart_height / -2)
						.rotate(270.0)
				)
				.append(
					Text::new()
						.with_text_anchor("middle")
						.with_dominant_baseline("middle")
						.append(yaxis_name)
				)
		);

		// build x axis legend
		group.push(
			Text::new()
				.with_text_anchor("middle")
				.with_dominant_baseline("middle")
				.append(
					TSpan::new()
						.with_x(chart_width / 2)
						.with_y(chart_margin_y - 15)
						.append(xaxis_name)
				)
				.append(
					TSpan::new()
						.with_x(chart_width / 2)
						.with_y(15 - chart_margin_y - chart_height)
						.append(xaxis_name)
				)
		);

		svg.push(group);

		// build tooltip
		let mut tt = Group::new()
			.with_id("tt")
			.with_transform(Transform::new().scale(0.0, 0.0));
		{
			let tip_straight_h = 5;
			let tip_curve_h = 2;
			let width = 80;
			let height = 50;
			let border_r = 5;
			let bottom_hline = width / 2 - tip_straight_h - 2 * tip_curve_h;
			let mut data = Data::new();
			data.move_to(0, 0)
				.line_by(tip_straight_h, -tip_straight_h)
				.quad_by(tip_curve_h, -tip_curve_h, 2 * tip_curve_h, -tip_curve_h)
				.horiz_line_by(bottom_hline)
				.arc_by(border_r, border_r, 0, false, false, border_r, -border_r)
				.vert_line_by(-height)
				.arc_by(border_r, border_r, 0, false, false, -border_r, -border_r)
				.horiz_line_by(-width)
				.arc_by(border_r, border_r, 0, false, false, -border_r, border_r)
				.vert_line_by(height)
				.arc_by(border_r, border_r, 0, false, false, border_r, border_r)
				.horiz_line_by(bottom_hline)
				.quad_by(tip_curve_h, 0, 2 * tip_curve_h, tip_curve_h)
				.close();
			tt.push(Path::new().with_d(data));
			tt.push(
				Text::new()
					.with_dominant_baseline("hanging")
					.with_transform(Transform::new().translate(
						width / -2,
						-height - tip_straight_h - tip_curve_h - border_r
					))
					.append(TSpan::new().with_id("ttp"))
					.append(TSpan::new().with_id("ttx").with_x(0).with_dy("1.2em"))
					.append(TSpan::new().with_id("tty").with_x(0).with_dy("1.2em"))
			);
		}
		svg.push(tt);

		let svg = if self.pretty {
			svg.to_string_pretty()
		} else {
			svg.to_string()
		};
		writeln!(out, "{}", svg)
	}
}
