use super::{OutputList, OutputPrinter};
use crate::int;
use std::io::{self, Write};

/// Default output printer.
pub struct ListPrinter;

impl OutputPrinter for ListPrinter {
	fn print<I, E, W>(self, list: OutputList<I>, mut out: W) -> io::Result<()>
	where
		I: IntoIterator<Item = (E, usize)>,
		E: IntoIterator<Item = (&'static str, int)>,
		W: Write
	{
		writeln!(out, "Output Variable Analysis:")?;
		for (key, stats) in list.stats() {
			writeln!(out, "  {key}: {stats:?}")?;
		}

		writeln!(out, "Samples:")?;
		let mut qty_buf = itoa::Buffer::new();
		let len_str = qty_buf.format(list.n);
		let qty_spacing = len_str.len();
		for (entry, qty) in list.samples {
			let qty_str = qty_buf.format(qty);
			let qty_str_len = qty_str.len();
			write!(out, "  ")?;
			for _ in 0 .. qty_spacing - qty_str_len {
				write!(out, " ")?;
			}
			write!(
				out,
				"{qty_str} ({:>6.2}%): {{ ",
				qty as f32 / list.n as f32 * 100.0
			)?;
			for (i, (key, value)) in entry.into_iter().enumerate() {
				if i > 0 {
					write!(out, ", ")?;
				}
				write!(out, "{key}: {value}")?;
			}
			writeln!(out, " }}")?;
		}
		Ok(())
	}
}
