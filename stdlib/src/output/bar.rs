use super::{OutputList, OutputPrinter, Svg};
use crate::int;
use std::{
	cmp::Ordering,
	collections::BTreeMap,
	fmt::Write as _,
	io::{self, Write},
	mem
};
use svgwriter::{
	tags::{Group, Path, Rect, Style, TSpan, TagWithPresentationAttributes as _, Text},
	Data, Transform
};

/// Output printer for SVG Bar Charts. Works best with one
/// output variable.
pub struct BarChartPrinter {
	/// Set to true to enable SVG/XML pretty printing.
	pretty: bool
}

impl BarChartPrinter {
	pub fn new(pretty: bool) -> Self {
		Self { pretty }
	}
}

#[derive(Default)]
struct YAxis {
	percentages: Vec<f32>,
	max: f32
}

impl YAxis {
	fn add_percentage(&mut self, percentage: f32) {
		self.percentages.push(percentage);
		self.max = self.max.max(percentage).ceil();
	}
}

#[derive(Clone, Copy, Eq)]
enum Range {
	Single(int),
	Range { start: int, len: int }
}

impl Range {
	fn start(&self) -> int {
		match self {
			Self::Single(value) => *value,
			Self::Range { start, .. } => *start
		}
	}

	fn end(&self) -> int {
		match self {
			Self::Single(value) => *value,
			Self::Range { start, len } => start + len - 1
		}
	}
}

impl PartialEq for Range {
	fn eq(&self, other: &Self) -> bool {
		self.start() == other.start() && self.end() == other.end()
	}
}

impl PartialOrd for Range {
	fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
		Some(self.cmp(other))
	}
}

impl Ord for Range {
	fn cmp(&self, other: &Self) -> Ordering {
		if self == other {
			Ordering::Equal
		} else if self.start() == other.start() {
			self.end().cmp(&other.end())
		} else {
			self.start().cmp(&other.start())
		}
	}
}

#[derive(Default)]
struct XAxis {
	values: BTreeMap<Range, usize>,
	max: Option<int>,
	min: Option<int>
}

impl XAxis {
	fn add_value(&mut self, value: int, qty: usize) {
		let range = Range::Single(value);
		if let Some(range_qty) = self.values.get_mut(&range) {
			*range_qty += qty;
		} else {
			self.values.insert(range, qty);
		}

		if self.max.map(|max| max < value).unwrap_or(true) {
			self.max = Some(value);
		}
		if self.min.map(|min| min > value).unwrap_or(true) {
			self.min = Some(value);
		}
	}

	fn with_step(&mut self, step: int) {
		let values = mem::take(&mut self.values);
		for (range, qty) in values {
			let value = match range {
				Range::Single(value) => value,
				_ => panic!("with_step may only be called once")
			};
			let offset = value - self.min.unwrap();
			let start = self.min.unwrap() + (offset / step) * step;
			let range = Range::Range { start, len: step };

			if let Some(range_qty) = self.values.get_mut(&range) {
				*range_qty += qty;
			} else {
				self.values.insert(range, qty);
			}
		}
	}
}

impl OutputPrinter for BarChartPrinter {
	fn print<I, E, W>(self, list: OutputList<I>, mut out: W) -> io::Result<()>
	where
		I: IntoIterator<Item = (E, usize)>,
		E: IntoIterator<Item = (&'static str, int)>,
		W: Write
	{
		writeln!(out, "<!--")?;
		writeln!(out, "Output Variable Analysis:")?;
		for (key, stats) in list.stats() {
			writeln!(out, "  {key}: {stats:?}")?;
		}
		writeln!(out, "-->")?;

		let mut yaxis = YAxis::default();
		let mut xaxis = XAxis::default();
		let mut xaxis_name = None;

		for (entry, qty) in list.samples {
			for (key, value) in entry {
				if xaxis_name.is_none() {
					xaxis_name = Some(key);
				} else if xaxis_name.unwrap() != key {
					continue;
				}

				xaxis.add_value(value, qty);
			}
		}
		let xaxis_name = xaxis_name.expect("Missing output variable");

		// group the xaxis together
		let mut xaxis_step = 1;
		while (xaxis.max.unwrap() - xaxis.min.unwrap()) / xaxis_step > 12 {
			xaxis_step += 1;
		}
		xaxis.with_step(xaxis_step);

		// build the yaxis
		for qty in xaxis.values.values().copied() {
			yaxis.add_percentage(qty as f32 / list.n as f32 * 100.0);
		}

		let chart_margin = 30;
		let chart_margin_x = 70;
		let chart_margin_y = 100;
		let axis_step_dist = 45;
		let bar_width = 20;
		let yaxis_steps = 7;
		// note that the number of datapoints is equivalent on both axis, therefore
		// we can use the yaxis to calculate the width
		let chart_width = (yaxis.percentages.len() as i32 + 1) * axis_step_dist;
		let chart_height = yaxis_steps * axis_step_dist + axis_step_dist / 2;
		let svg_width = chart_width + chart_margin + chart_margin_x;
		let svg_height = chart_height + chart_margin + chart_margin_y;
		let mut svg = Svg::new(svg_width, svg_height);

		// add svg style
		let mut style = concat!(
			"*{font-family:\"Source Serif 4\",serif}",
			".ui{stroke:#000;stroke-width:1px;fill:#000;font-size:12pt}",
			".ui text,.bg,.b{stroke-width:0}",
			".p{font-size:10pt}",
			".bg{fill:#fff}",
			".b{stroke:#f60;fill:#f60}",
			// dark theme
			"@media(prefers-color-scheme: dark){",
			".ui{stroke:#ccc;fill:#ccc}",
			".bg{fill:#222}",
			"}"
		)
		.to_owned();
		// allow svg to be printed to pdf by headless chromium
		write!(
			style,
			"@page{{margin:0;size:calc(20cm*{svg_width}/{svg_height}) 20cm}}"
		)
		.unwrap();
		svg.push(Style::new().append(style));

		// add svg background
		svg.push(
			Rect::new()
				.with_class("bg")
				.with_x(0)
				.with_y(0)
				.with_width(chart_width + chart_margin + chart_margin_x)
				.with_height(chart_height + chart_margin + chart_margin_y)
		);

		let mut group = Group::new()
			.with_transform(
				Transform::new().translate(chart_margin_x, chart_height + chart_margin)
			)
			.with_class("ui");

		// build vertical bars
		let mut bars_data = Data::new();
		let mut bars_text = Text::new().with_text_anchor("middle");
		for (i, percentage) in yaxis.percentages.iter().enumerate() {
			let offset = (i + 1) as i32 * axis_step_dist;
			let height = percentage / yaxis.max * (yaxis_steps * axis_step_dist) as f32;
			bars_data
				.move_to(offset - bar_width / 2, 0)
				.horiz_line_by(bar_width)
				.vert_line_by(-height)
				.horiz_line_by(-bar_width)
				.close();
			bars_text.push(
				TSpan::new()
					.with_x(offset)
					.with_y(-height - 5.0)
					.append(format!("{percentage:.2}%"))
			);
		}
		group.push(Path::new().with_d(bars_data).with_class("b"));
		group.push(bars_text.with_class("b p"));

		// build y axis line
		let mut axis_line = Data::new();
		axis_line
			.move_to(0, -chart_height)
			.line_by(3, 5)
			.horiz_line_by(-6)
			.close()
			.vert_line_to(0);
		for (i, _) in (axis_step_dist .. chart_height)
			.step_by(axis_step_dist as usize)
			.enumerate()
		{
			axis_line
				.move_by((i == 0).then_some(-3).unwrap_or(-6), -axis_step_dist)
				.horiz_line_by(6);
		}

		// build x axis line
		axis_line
			.move_to(chart_width, 0)
			.line_by(-5, 3)
			.vert_line_by(-6)
			.close()
			.horiz_line_to(0);
		for (i, _) in (axis_step_dist .. chart_width)
			.step_by(axis_step_dist as usize)
			.enumerate()
		{
			axis_line
				.move_by(axis_step_dist, (i == 0).then_some(-3).unwrap_or(-6))
				.vert_line_by(6);
		}
		group.push(Path::new().with_d(axis_line).with_stroke_linejoin("arcs"));

		// build y axis descriptions
		let mut yaxis_text = Text::new()
			.with_text_anchor("end")
			.with_dominant_baseline("middle");
		for i in 0 .. yaxis_steps {
			yaxis_text.push(
				TSpan::new()
					.with_x(-5)
					.with_y((i as i32 + 1) * -axis_step_dist)
					.append(format!(
						"{:.2}%",
						yaxis.max / yaxis_steps as f32 * (i + 1) as f32
					))
			);
		}
		group.push(yaxis_text);

		// build x axis descriptions
		let mut xaxis_text = Text::new()
			.with_class("b")
			.with_text_anchor("middle")
			.with_dominant_baseline("hanging");
		for (i, range) in xaxis.values.keys().copied().enumerate() {
			let x = (i + 1) as i32 * axis_step_dist;
			let y = 5;
			match range {
				Range::Single(value) => {
					xaxis_text.push(
						TSpan::new()
							.with_x(x)
							.with_y(y)
							.append(format!("{}", value))
					);
				},
				Range::Range { start, len } => {
					xaxis_text.push(
						TSpan::new()
							.with_x(x)
							.with_y(y)
							.append(format!("{}", start))
					);
					xaxis_text.push(TSpan::new().with_x(x).with_dy(".8em").append("-"));
					xaxis_text.push(
						TSpan::new()
							.with_x(x)
							.with_dy("1em")
							.append(format!("{}", start + len - 1))
					);
				}
			}
		}
		group.push(xaxis_text);

		// build y axis legend
		let mut ylegend = Text::new().with_x(-10).with_y(-chart_height - 5);
		ylegend.push(format!("Percentage of {} samples", list.n));
		group.push(ylegend);

		// build x axis legend
		let mut xlegend = Text::new()
			.with_x(0)
			.with_y(chart_margin_y - chart_margin)
			.with_dominant_baseline("hanging");
		xlegend.push("Legend: ");
		xlegend.push(
			TSpan::new()
				.with_class("b")
				.append(format!("{}", xaxis.min.unwrap_or(1)))
		);
		xlegend.push(format!(": {xaxis_name}"));
		group.push(xlegend);

		svg.push(group);
		let svg = if self.pretty {
			svg.to_string_pretty()
		} else {
			svg.to_string()
		};
		writeln!(out, "{}", svg)
	}
}
