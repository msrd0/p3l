use rand::{
	distributions::{Distribution as _, Uniform},
	rngs::OsRng,
	Rng
};
use std::{
	fmt::{self, Debug, Formatter, Write as _},
	panic::panic_any
};
pub use std::{
	option::Option,
	primitive::{bool, isize as int}
};

mod output;
pub use output::{
	BarChartPrinter, BubbleChartPrinter, ListPrinter, OutputList, OutputPrinter,
	OutputPrinterAny, OutputPrinterTwoVars, OutputTarget
};

#[doc(hidden)]
pub struct FnNoReturn {
	msg: String
}

/// Called as the last part of every function to abnormally terminate in case there is a
/// missing return statement.
#[track_caller]
pub fn fn_no_return(fun_ident: &str) -> FnNoReturn {
	FnNoReturn {
		msg: format!(
			r#"Function `{}` has finished without returning a value, but it declares a return type.

Help: The function was called with the following arguments:"#,
			fun_ident
		)
	}
}

impl FnNoReturn {
	pub fn add_input<T>(mut self, key: &'static str, value: &T) -> Self
	where
		T: Debug
	{
		write!(self.msg, "\n\t{key}: {value:#?}").unwrap();
		self
	}

	pub fn panic(self) -> ! {
		panic_any(self.msg)
	}
}

/// Return the default (pseudo) random number generator.
fn rng() -> impl Rng {
	OsRng::default()
}

/// Used for input argument parsing.
#[derive(Debug, Clone, Copy)]
pub enum ArgValue {
	Value(int),
	Range { min: int, max: int }
}

impl ArgValue {
	pub fn get_value(self) -> int {
		match self {
			Self::Value(value) => value,
			Self::Range { min, max } => {
				Uniform::new_inclusive(min, max).sample(&mut rng())
			},
		}
	}
}

/// The result of a coin flip.
pub enum Coin {
	Head,
	Tail
}

impl Coin {
	/// Flip a coin, returning [`Coin::Head`] with probability `prob`.
	pub fn flip(prob: f32) -> Self {
		debug_assert!(prob > 0.0);
		debug_assert!(prob < 1.0);

		if rng().gen::<f32>() < prob {
			Self::Head
		} else {
			Self::Tail
		}
	}
}

/// The `Rc` reference-counting smart pointer of the language.
pub struct Rc<T>(std::rc::Rc<std::cell::RefCell<T>>);

impl<T> Rc<T> {
	pub fn new(inner: T) -> Self {
		Self(std::rc::Rc::new(std::cell::RefCell::new(inner)))
	}
}

impl<T> Rc<T> {
	pub fn into_inner(this: Rc<T>) -> T
	where
		T: Clone
	{
		match std::rc::Rc::try_unwrap(this.0) {
			Ok(cell) => cell.into_inner(),
			Err(rc) => rc.borrow().clone()
		}
	}
}

impl<T> Rc<T> {
	pub fn with_mut<F>(&mut self, callback: F)
	where
		F: FnOnce(&mut T)
	{
		let mut this = self.0.borrow_mut();
		callback(&mut this);
	}
}

impl<T> Clone for Rc<T> {
	fn clone(&self) -> Self {
		Self(std::rc::Rc::clone(&self.0))
	}
}

impl<T: Debug> Debug for Rc<T> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.debug_tuple("Rc")
			.field(&DebugPtr(self.0.as_ptr()))
			.field(&self.0.borrow())
			.finish()
	}
}

struct DebugPtr<T>(*const T);

impl<T> Debug for DebugPtr<T> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{:#X}", self.0 as usize)
	}
}
