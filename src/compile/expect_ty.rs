use crate::{ast::Type, diagnostic::Diagnostic};
use proc_macro2::Span;
use syn::spanned::Spanned as _;

pub(super) fn expect_ty(
	expected_ty: &Type,
	expression_ty: &Type,
	expression_span: Span
) -> Result<(), Diagnostic> {
	if expected_ty == expression_ty {
		Ok(())
	} else {
		Err(Diagnostic::type_mismatch(
			expected_ty,
			expected_ty.span(),
			expression_ty,
			expression_span
		))
	}
}

pub(super) fn expect_bool(
	expr_ty: &Type,
	expr_span: Span,
	why_span: Span
) -> Result<(), Diagnostic> {
	match expr_ty {
		Type::Bool(_) => Ok(()),
		_ => Err(Diagnostic::type_mismatch(
			"bool", why_span, expr_ty, expr_span
		))
	}
}

pub(super) fn expect_int(
	expr_ty: &Type,
	expr_span: Span,
	why_span: Span
) -> Result<(), Diagnostic> {
	match expr_ty {
		Type::Int(_) => Ok(()),
		_ => Err(Diagnostic::type_mismatch(
			"int", why_span, expr_ty, expr_span
		))
	}
}

pub(super) fn expect_tuple_exact(
	ty: &Type,
	elems_len: usize,
	expr_span: Span,
	why_span: Span
) -> Result<(), Diagnostic> {
	match ty {
		Type::Tuple(tuple) if tuple.elems.len() == elems_len => Ok(()),
		Type::Tuple(tuple) => Err(Diagnostic::new(
			expr_span,
			"Type Mismatch",
			format!("but expression has {elems_len} elements")
		)
		.with_label(
			ty.span(),
			format!("Expected tuple with {} elements,", tuple.elems.len())
		)),
		_ => Err(Diagnostic::type_mismatch(
			ty,
			expr_span,
			format!("({})", ", ".repeat(elems_len)),
			why_span
		))
	}
}

pub(super) fn expect_tuple_at_least(
	ty: &Type,
	elems_len: usize,
	expr_span: Span,
	why_span: Span
) -> Result<(), Diagnostic> {
	match ty {
		Type::Tuple(tuple) if tuple.elems.len() >= elems_len => Ok(()),
		Type::Tuple(tuple) => Err(Diagnostic::new(
			expr_span,
			"Type Mismatch",
			format!("but expression requires at least {elems_len} elements")
		)
		.with_label(
			ty.span(),
			format!("Found tuple with {} elements,", tuple.elems.len())
		)),
		_ => Err(Diagnostic::type_mismatch(
			format!("({}..)", "_, ".repeat(elems_len)),
			why_span,
			ty,
			expr_span
		))
	}
}

pub(super) fn expect_rc(
	expr_ty: &Type,
	expr_span: Span,
	why_span: Span
) -> Result<(), Diagnostic> {
	match expr_ty {
		Type::Rc(_) => Ok(()),
		_ => Err(Diagnostic::type_mismatch(
			"Rc<_>", why_span, expr_ty, expr_span
		))
	}
}

pub(super) fn expect_option(
	expr_ty: &Type,
	expr_span: Span,
	why_span: Span
) -> Result<(), Diagnostic> {
	match expr_ty {
		Type::Option(_) => Ok(()),
		_ => Err(Diagnostic::type_mismatch(
			"Option<_>",
			why_span,
			expr_ty,
			expr_span
		))
	}
}

pub(super) fn expect_rc_ctor(
	expected_ty: &Type,
	expr_span: Span
) -> Result<(), Diagnostic> {
	match expected_ty {
		Type::Rc(_) => Ok(()),
		_ => Err(Diagnostic::type_mismatch(
			expected_ty,
			expected_ty.span(),
			"Rc<_>",
			expr_span
		))
	}
}

pub(super) fn expect_option_ctor(
	expected_ty: &Type,
	expr_span: Span
) -> Result<(), Diagnostic> {
	match expected_ty {
		Type::Option(_) => Ok(()),
		_ => Err(Diagnostic::type_mismatch(
			expected_ty,
			expected_ty.span(),
			"Option<_>",
			expr_span
		))
	}
}
