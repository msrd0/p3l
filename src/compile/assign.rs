use super::{expr::expand_expr_with_type, resolve_ty::resolve_tycustom, State};
use crate::{
	ast::{Assignment, Expression, Type},
	diagnostic::Diagnostic,
	verbose
};
use std::{borrow::Cow, iter::Peekable};
use syn::{spanned::Spanned, Member};

fn cont<I>(
	state: &State<'_>,
	current_ty: &Type,
	mut remaining: Peekable<I>,
	expr: Expression
) -> Result<(Cow<'static, str>, Cow<'static, str>), Diagnostic>
where
	I: Iterator<Item = Member>
{
	if remaining.peek().is_none() {
		if verbose() {
			eprintln!("[assign] expanding assignment with type {current_ty} and expression {expr}");
		}
		let expr = expand_expr_with_type(state, current_ty, expr)?;
		return Ok((" = __temp_value;".into(), expr));
	}

	Ok(match current_ty {
		Type::Bool(_) => {
			return Err(Diagnostic::no_such_field(
				remaining.next().unwrap().span(),
				"bool is a primitive type and does not have any fields"
			));
		},

		Type::Int(_) => {
			return Err(Diagnostic::no_such_field(
				remaining.next().unwrap().span(),
				"int is a primitive type and does not have any fields"
			));
		},

		Type::Tuple(tuple) => {
			let index = match remaining.next().unwrap() {
				Member::Named(named) => {
					return Err(Diagnostic::no_such_field(
						named.span(),
						"tuples don't have named fields"
					)
					.with_help("To access the first element in a tuple, use `.0`"));
				},
				Member::Unnamed(index) if index.index as usize >= tuple.elems.len() => {
					return Err(Diagnostic::no_such_field(
						index.span(),
						"this tuple does not have enough elements"
					)
					.with_note("The first element in a tuple has index 0"));
				},
				Member::Unnamed(index) => index.index as usize
			};
			let (path, expr) = cont(state, current_ty.nth_ty(index), remaining, expr)?;
			(format!(".{index}{path}").into(), expr)
		},

		Type::Rc(rc) => {
			let (path, expr) = cont(state, &rc.inner_ty, remaining, expr)?;
			(
				format!(".with_mut(|__temp| {{ __temp{path} }});").into(),
				expr
			)
		},

		Type::Option(_) => {
			return Err(Diagnostic::no_such_field(
				remaining.next().unwrap().span(),
				"Option does not have any fields"
			));
		},

		Type::Custom(custom) => {
			let ty = resolve_tycustom(state, custom)?;
			return cont(state, &ty, remaining, expr);
		},

		Type::Any => unimplemented!()
	})
}

pub(super) fn expand_assign(
	state: &State<'_>,
	assign: Assignment
) -> Result<String, Diagnostic> {
	let mut lhs = assign.lhs.into_iter().peekable();
	let first = match lhs.next().unwrap() {
		Member::Named(ident) => ident,
		Member::Unnamed(_) => unreachable!()
	};
	let first_ty = state.var(&first)?;
	let (path, expr) = cont(state, first_ty, lhs, assign.rhs)?;
	Ok(format!("{{ let __temp_value = {expr}; {first}{path} }}",))
}
