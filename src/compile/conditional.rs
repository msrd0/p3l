use super::{
	expand_block,
	expect_ty::expect_option,
	expr::{expand_expr, expand_expr_with_type},
	State
};
use crate::{
	ast::{Condition, ElseBranch, If, Type, TypePrimitive, While},
	diagnostic::Diagnostic
};
use proc_macro2::Span;
use std::borrow::Cow;
use syn::Token;

pub(super) fn expand_condition(
	state: &mut State<'_>,
	condition: Condition,
	if_span: Span
) -> Result<Cow<'static, str>, Diagnostic> {
	Ok(match condition {
		Condition::Bool(expr) => expand_expr_with_type(
			state,
			&Type::Bool(TypePrimitive { span: if_span }),
			expr
		)?,
		Condition::LetSome {
			some_span,
			ident,
			expr,
			..
		} => {
			let expr = expand_expr(state, expr)?;
			expect_option(&expr.ty, expr.expr_span, some_span)?;
			let buf = format!("let ::stdlib::Option::Some(mut {ident}) = {}", expr.code);
			let expr_ty = expr.ty.into_owned();
			state.add_variable(ident, expr_ty.into_inner_ty());
			buf.into()
		}
	})
}

fn expand_else_branch(
	state: &State<'_>,
	else_branch: Option<(Token![else], ElseBranch)>
) -> Result<String, Diagnostic> {
	let mut buf = String::new();
	if let Some((_, else_branch)) = else_branch {
		buf += " else ";
		match else_branch {
			ElseBranch::If(else_if) => {
				buf += "{";
				buf += &expand_if(state, *else_if)?;
				buf += "}";
			},
			ElseBranch::Block(block) => {
				buf += &expand_block(state, block)?;
			}
		}
	}
	Ok(buf)
}

pub(super) fn expand_if(state: &State<'_>, if_expr: If) -> Result<String, Diagnostic> {
	let mut sub = state.substate();
	let condition = expand_condition(&mut sub, if_expr.condition, if_expr.if_token.span)?;
	let block = expand_block(&sub, if_expr.then_branch)?;

	let mut buf = format!("if {condition} {block}");
	buf += &expand_else_branch(state, if_expr.else_branch)?;
	Ok(buf)
}

pub(super) fn expand_while(
	state: &State<'_>,
	while_expr: While
) -> Result<String, Diagnostic> {
	let mut sub = state.substate().into_loop();
	let condition =
		expand_condition(&mut sub, while_expr.condition, while_expr.while_token.span)?;
	let block = expand_block(&sub, while_expr.loop_body)?;

	Ok(format!("while {condition} {block}"))
}
