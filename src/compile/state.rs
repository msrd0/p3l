use super::resolve_ty::{resolve_ty, GenericsMap};
use crate::{
	ast::{Fn, Generics, Type, TypeDef, TypeTuple},
	diagnostic::Diagnostic,
	verbose
};
use indexmap::IndexMap;
use proc_macro2::{Ident, Span};
use std::{
	borrow::Cow,
	cell::RefCell,
	collections::{HashMap, HashSet},
	fmt::Write as _
};
use syn::spanned::Spanned;

fn mangled_fn_name(function: &Fn, generics: &Option<Generics<Type>>) -> String {
	let mut ident = format!("fun_{}", function.ident);
	if let Some(g) = generics {
		for ty in &g.params {
			write!(ident, "_{}", ty.mangled()).unwrap();
		}
	}
	ident
}

struct FunctionMap {
	/// A list of function definitions.
	functions: Vec<Fn>,

	/// A mapping of function identifier to the function definition index.
	idents: HashMap<Ident, usize>,

	/// A list of generics for this function that are being used with the
	/// mangled name of the function.
	used_generics: RefCell<Vec<IndexMap<Option<Generics<Type>>, String>>>,

	/// A list of function instances that have been emitted already.
	emitted_generics: Vec<HashSet<usize>>
}

impl FunctionMap {
	/// Create a new function map with these available functions of which
	/// none have any instances that are being used.
	fn new(functions: Vec<Fn>) -> Self {
		let idents = functions
			.iter()
			.enumerate()
			.map(|(i, fun)| (fun.ident.clone(), i))
			.collect();
		let len = functions.len();
		Self {
			functions,
			idents,
			used_generics: RefCell::new(vec![IndexMap::new(); len]),
			emitted_generics: vec![HashSet::new(); len]
		}
	}

	/// Create a new, empty function map.
	fn new_empty() -> Self {
		Self {
			functions: Vec::new(),
			idents: HashMap::new(),
			used_generics: RefCell::new(Vec::new()),
			emitted_generics: Vec::new()
		}
	}

	/// Get an instance of a function and return its mangled name. If no
	/// function with said ident exists, `None` is being returned.
	fn get(
		&self,
		ident: &Ident,
		generics: &Option<Generics<Type>>
	) -> Option<(&Fn, String)> {
		let idx = *self.idents.get(ident)?;
		if !self
			.used_generics
			.borrow()
			.get(idx)
			.unwrap()
			.contains_key(generics)
		{
			self.used_generics
				.borrow_mut()
				.get_mut(idx)
				.unwrap()
				.insert(
					generics.clone(),
					mangled_fn_name(&self.functions[idx], generics)
				);
		}
		Some((
			&self.functions[idx],
			self.used_generics
				.borrow()
				.get(idx)
				.unwrap()
				.get(generics)
				.unwrap()
				.clone()
		))
	}
}

pub(super) struct FunctionIndex {
	fun_idx: usize,
	inst_idx: usize
}

pub(super) struct State<'a> {
	/// The parent state, or `None`.
	parent: Option<&'a State<'a>>,

	/// True if this substate represents a loop body.
	is_loop: bool,

	/// A mapping of custom type identifier to the corresponding typedef.
	types: HashMap<Ident, TypeDef>,

	/// A mapping of generic ident to concrete instance. Used only for
	/// function.
	generic_types: HashMap<Ident, Type>,

	/// When the state represents a function, this stores the return
	/// type of the function.
	return_ty: Option<Type>,

	/// All the functions, which is a bit more complex since we need to
	/// track the generic instances that are being used in the program.
	functions: FunctionMap,

	/// A mapping of variable identifier to the type of the variable.
	variables: HashMap<Ident, Type>
}

impl State<'static> {
	pub(super) fn new(types: Vec<TypeDef>, functions: Vec<Fn>) -> Self {
		Self {
			parent: None,
			is_loop: false,
			types: types.into_iter().map(|ty| (ty.ident.clone(), ty)).collect(),
			generic_types: HashMap::new(),
			return_ty: None,
			functions: FunctionMap::new(functions),
			variables: HashMap::new()
		}
	}

	pub(super) fn types(&self) -> impl Iterator<Item = &TypeDef> {
		if self.parent.is_some() {
			panic!("This function may only be called on the root state");
		}
		self.types.values()
	}

	pub(super) fn next_function_instance(&mut self) -> Option<FunctionIndex> {
		if self.parent.is_some() {
			panic!("This function may only be called on the root state");
		}

		for (fun_idx, instances) in
			self.functions.used_generics.get_mut().iter().enumerate()
		{
			for (inst_idx, _) in instances.iter().enumerate() {
				if self.functions.emitted_generics[fun_idx].contains(&inst_idx) {
					continue;
				}
				self.functions.emitted_generics[fun_idx].insert(inst_idx);
				return Some(FunctionIndex { fun_idx, inst_idx });
			}
		}

		None
	}

	pub(super) fn with_function_instance<F, T>(
		&self,
		idx: FunctionIndex,
		callback: F
	) -> T
	where
		F: FnOnce(&Fn, Option<Generics<Type>>, String) -> T
	{
		if self.parent.is_some() {
			panic!("This function may only be called on the root state");
		}

		let used_generics = self.functions.used_generics.borrow();

		let fun = &self.functions.functions[idx.fun_idx];
		let (generics, ident) = used_generics
			.get(idx.fun_idx)
			.unwrap()
			.get_index(idx.inst_idx)
			.unwrap();
		let generics = generics.clone();
		let ident = ident.clone();

		// since the callback might need to add another function instance
		// or do a lookup, we cannot keep the borrow past this point.
		drop(used_generics);
		callback(fun, generics, ident)
	}
}

impl<'a> State<'a> {
	pub(super) fn is_loop(&self) -> bool {
		self.is_loop || self.parent.map(State::is_loop).unwrap_or(false)
	}

	pub(super) fn into_loop(mut self) -> Self {
		self.is_loop = true;
		self
	}

	pub(super) fn ty(&self, ident: &Ident) -> Result<&TypeDef, Diagnostic> {
		if let Some(ty) = self.types.get(ident) {
			return Ok(ty);
		}
		match self.parent {
			Some(parent) => parent.ty(ident),
			None => Err(Diagnostic::new(
				ident.span(),
				"Unknown type",
				"This type has not been defined"
			))
		}
	}

	fn has_generic_types(&self) -> bool {
		!self.generic_types.is_empty()
			|| self
				.parent
				.map(|parent| parent.has_generic_types())
				.unwrap_or(false)
	}

	pub(super) fn generic_types(&self) -> &HashMap<Ident, Type> {
		if !self.generic_types.is_empty() {
			return &self.generic_types;
		}

		match self.parent.as_ref() {
			Some(parent) => parent.generic_types(),
			None => &self.generic_types
		}
	}

	pub(super) fn add_ty(&mut self, ident: Ident, ty: Type) {
		if self.generic_types.is_empty() {
			// ensure that only one (sub)state in the chain has generic types
			if let Some(parent) = self.parent.as_ref() {
				if parent.has_generic_types() {
					panic!("One of the parent state already has generic types");
				}
			}
		}

		self.generic_types.insert(ident, ty);
	}

	pub(super) fn return_ty(&self) -> Option<&Type> {
		self.return_ty
			.as_ref()
			.or_else(|| self.parent.and_then(|parent| parent.return_ty()))
	}

	pub(super) fn set_return_ty(&mut self, ty: Type) {
		if self.return_ty().is_some() {
			panic!(
				"This state or one of its parent states already has a return type set"
			);
		}

		self.return_ty = Some(ty);
	}

	pub(super) fn fun(
		&self,
		ident: &Ident,
		generics: &Option<Generics<Type>>
	) -> Result<FunctionInstance<'_>, Diagnostic> {
		let generics = generics.as_ref().map(|generics| Generics {
			lt_token: generics.lt_token,
			params: generics
				.params
				.iter()
				.map(|param| resolve_ty(param, self.generic_types()).into_owned())
				.collect(),
			gt_token: generics.gt_token
		});

		if let Some((fun, fun_ident)) = self.functions.get(ident, &generics) {
			match (fun.generics.as_ref(), generics.as_ref()) {
				(None, None) => {},
				(Some(fun_generics), Some(generics))
					if fun_generics.params.len() == generics.params.len() => {},
				_ => return Err(Diagnostic::new(
					ident.span(),
					"Mismatched generics",
					"The generics at the call site differ with the function declaration"
				))
			};
			return Ok(FunctionInstance {
				fun,
				generics,
				ident: fun_ident
			});
		}
		match self.parent {
			Some(parent) => parent.fun(ident, &generics),
			None => Err(Diagnostic::new(
				ident.span(),
				"Unknown function",
				"This function has not been defined"
			))
		}
	}

	pub(super) fn var(&self, ident: &Ident) -> Result<&Type, Diagnostic> {
		if let Some(ty) = self.variables.get(ident) {
			return Ok(ty);
		}
		match self.parent {
			Some(parent) => parent.var(ident),
			None => Err(Diagnostic::new(
				ident.span(),
				"No such variable",
				"Variable not defined in scope"
			))
		}
	}

	pub(super) fn add_variable(&mut self, ident: Ident, ty: Type) {
		if verbose() {
			eprintln!("[state] adding variable `{ident}` with type `{ty}`");
		}
		self.variables.insert(ident, ty);
	}

	pub(super) fn substate<'b>(&'b self) -> State<'b>
	where
		'a: 'b
	{
		State {
			parent: Some(self),
			is_loop: false,
			types: HashMap::new(),
			generic_types: HashMap::new(),
			return_ty: None,
			functions: FunctionMap::new_empty(),
			variables: HashMap::new()
		}
	}
}

pub(super) struct FunctionInstance<'a> {
	fun: &'a Fn,
	generics: Option<Generics<Type>>,
	pub ident: String
}

impl Spanned for FunctionInstance<'_> {
	fn span(&self) -> Span {
		self.fun.ident.span()
	}
}

impl FunctionInstance<'_> {
	pub(super) fn inputs_len(&self) -> usize {
		self.fun.inputs.len()
	}

	fn generics(&self) -> impl GenericsMap<Out = &Type> + Copy {
		self.fun
			.generics
			.as_ref()
			.and_then(|fg| self.generics.as_ref().map(|ig| (fg, ig)))
	}

	pub(super) fn inputs(&self) -> impl Iterator<Item = Cow<'_, Type>> {
		let generics = self.generics();
		self.fun
			.inputs
			.iter()
			.map(move |arg| resolve_ty(&arg.ty, generics))
	}

	pub(super) fn output(&self) -> Cow<'_, Type> {
		let generics = self.generics();
		self.fun
			.output
			.as_ref()
			.map(|(_, ty)| resolve_ty(ty, generics))
			.unwrap_or_else(|| Type::Tuple(TypeTuple::default()).into())
	}
}
