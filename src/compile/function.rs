use super::{expand_stmt, resolve_ty::resolve_ty, ty::expand_type, State};
use crate::{
	ast::{Fn, Generics, Type, TypeTuple},
	diagnostic::Diagnostic
};
use std::fmt::Write as _;

pub(super) fn expand_function(
	buf: &mut String,
	state: &State<'_>,
	fun: &Fn,
	generics: Option<Generics<Type>>,
	ident: String
) -> Result<(), Diagnostic> {
	let mut sub = state.substate();
	if let Some((fun_generics, inst_generics)) = fun
		.generics
		.as_ref()
		.and_then(|fg| generics.as_ref().map(|ig| (fg, ig)))
	{
		for (ident, ty) in fun_generics.params.iter().zip(inst_generics.params.iter()) {
			sub.add_ty(ident.clone(), ty.clone());
		}
	}

	// function signature
	*buf += "#[allow(non_snake_case)]\n";
	write!(buf, "fn {ident}").unwrap();
	*buf += "(";
	for arg in &fun.inputs {
		write!(buf, "mut {}: {}, ", arg.ident, expand_type(&sub, &arg.ty)?).unwrap();
		sub.add_variable(
			arg.ident.clone(),
			resolve_ty(&arg.ty, sub.generic_types()).into_owned()
		);
	}
	*buf += ")";
	if let Some((_, out)) = fun.output.as_ref() {
		write!(buf, " -> {}", expand_type(&sub, out)?).unwrap();
		sub.set_return_ty(out.clone());
	} else {
		sub.set_return_ty(Type::Tuple(TypeTuple::default()))
	}

	// function body
	*buf += " {\n";
	for s in &fun.block.stmts {
		*buf += &expand_stmt(&mut sub, s.clone())?;
	}
	if fun.output.is_some() {
		*buf += "\n#[allow(unreachable_code)]\n{\n";
		write!(buf, "::stdlib::fn_no_return({:?})", fun.ident.to_string()).unwrap();
		for arg in &fun.inputs {
			write!(
				buf,
				".add_input(::core::stringify!({ident}), &{ident})",
				ident = arg.ident
			)
			.unwrap();
		}
		*buf += ".panic()\n}\n";
	}
	*buf += "}\n";

	Ok(())
}
