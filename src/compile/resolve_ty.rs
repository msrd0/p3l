use super::State;
use crate::{
	ast::{Generics, Type, TypeCustom, TypePredef, TypeTuple},
	diagnostic::Diagnostic
};
use proc_macro2::Ident;
use std::{borrow::Cow, collections::HashMap};

pub(super) trait GenericsMap {
	type Out;

	fn get(&self, ident: &Ident) -> Option<Self::Out>;
}

impl<'a> GenericsMap for (&Generics<Ident>, &'a Generics<Type>) {
	type Out = &'a Type;

	fn get(&self, ident: &Ident) -> Option<Self::Out> {
		self.0
			.params
			.iter()
			.position(|param| param == ident)
			.map(|index| self.1.params.iter().nth(index).unwrap())
	}
}

impl<'a> GenericsMap for &'a HashMap<Ident, Type> {
	type Out = &'a Type;

	fn get(&self, ident: &Ident) -> Option<Self::Out> {
		HashMap::get(self, ident)
	}
}

impl<T: GenericsMap> GenericsMap for Option<T> {
	type Out = T::Out;

	fn get(&self, ident: &Ident) -> Option<Self::Out> {
		self.as_ref().and_then(|this| this.get(ident))
	}
}

impl<T, U> GenericsMap for (T, U)
where
	T: GenericsMap,
	U: GenericsMap<Out = T::Out>
{
	type Out = T::Out;

	fn get(&self, ident: &Ident) -> Option<Self::Out> {
		self.0.get(ident).or_else(|| self.1.get(ident))
	}
}

fn resolve_typredef<'a, T>(ty: &'a TypePredef, generics: T) -> TypePredef
where
	T: GenericsMap<Out = &'a Type> + Copy
{
	TypePredef {
		span: ty.span,
		lt_token: ty.lt_token,
		inner_ty: Box::new(resolve_ty(&ty.inner_ty, generics).into_owned()),
		gt_token: ty.gt_token
	}
}

pub(super) fn resolve_ty<'a, T>(ty: &'a Type, generics: T) -> Cow<'a, Type>
where
	T: GenericsMap<Out = &'a Type> + Copy
{
	match ty {
		Type::Bool(_) | Type::Int(_) => ty.into(),
		Type::Tuple(tuple) => Type::Tuple(TypeTuple {
			paren_token: tuple.paren_token,
			elems: tuple
				.elems
				.iter()
				.map(|elem| resolve_ty(elem, generics).into_owned())
				.collect()
		})
		.into(),
		Type::Rc(rc) => Type::Rc(resolve_typredef(rc, generics)).into(),
		Type::Option(option) => Type::Option(resolve_typredef(option, generics)).into(),
		Type::Custom(custom) => {
			if let Some(resolved_ty) = generics.get(&custom.ident) {
				resolved_ty.into()
			} else {
				Type::Custom(TypeCustom {
					ident: custom.ident.clone(),
					generics: custom.generics.as_ref().map(|g| Generics {
						lt_token: g.lt_token,
						params: g
							.params
							.iter()
							.map(|ty| resolve_ty(ty, generics).into_owned())
							.collect(),
						gt_token: g.gt_token
					})
				})
				.into()
			}
		},
		Type::Any => Type::Any.into()
	}
}

pub(super) fn resolve_tycustom<'a, 'b: 'a>(
	state: &'a State<'b>,
	custom: &'a TypeCustom
) -> Result<Cow<'a, Type>, Diagnostic> {
	if custom.generics.is_none() {
		if let Some(resolved_ty) = state.generic_types().get(&custom.ident) {
			return Ok(resolved_ty.into());
		}
	}
	let inner_ty = state.ty(&custom.ident)?;
	Ok(resolve_ty(
		&inner_ty.ty,
		inner_ty.generics.as_ref().and_then(|def_generics| {
			custom
				.generics
				.as_ref()
				.map(|generics| (def_generics, generics))
		})
	))
}
