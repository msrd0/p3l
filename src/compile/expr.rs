use super::{
	expect_ty::{
		expect_bool, expect_int, expect_option_ctor, expect_rc, expect_rc_ctor,
		expect_tuple_at_least, expect_tuple_exact, expect_ty
	},
	resolve_ty::{resolve_ty, resolve_tycustom},
	State
};
use crate::{
	ast::{Expression, Type, TypePredef, TypeTuple},
	diagnostic::Diagnostic
};
use proc_macro2::Span;
use std::borrow::Cow;
use syn::{punctuated::Punctuated, spanned::Spanned};

pub(super) struct ExpandedExpression<'a> {
	pub(super) code: Cow<'static, str>,
	pub(super) expr_span: Span,
	pub(super) ty: Cow<'a, Type>
}

impl<'a, C, S, T> From<(C, S, T)> for ExpandedExpression<'a>
where
	C: Into<Cow<'static, str>>,
	S: Spanned,
	T: Into<Cow<'a, Type>>
{
	fn from((code, expr_span, ty): (C, S, T)) -> Self {
		Self {
			code: code.into(),
			expr_span: expr_span.span(),
			ty: ty.into()
		}
	}
}

pub(super) fn expand_expr<'a>(
	state: &'a State<'_>,
	expr: Expression
) -> Result<ExpandedExpression<'a>, Diagnostic> {
	let span = expr.span();
	let expanded: ExpandedExpression<'a> = match expr {
		Expression::Term { expr, .. } => {
			let expr = expand_expr(state, *expr)?;
			(format!("({})", expr.code), span, expr.ty).into()
		},

		Expression::Ident(ident) => (
			format!("::core::clone::Clone::clone(&{ident})"),
			span,
			state.var(&ident)?
		)
			.into(),

		Expression::Bool(lit) => {
			let code = match lit.value {
				true => "true",
				false => "false"
			};
			(code, span, Type::Bool(Default::default())).into()
		},

		Expression::Literal(lit) => {
			(lit.to_string(), span, Type::Int(Default::default())).into()
		},

		Expression::BoolNegation { not_token, expr } => {
			let bool = Type::Bool(Default::default());
			let expr = expand_expr(state, *expr)?;
			expect_bool(&expr.ty, expr.expr_span, not_token.span)?;
			(format!("!({})", expr.code), span, bool).into()
		},

		Expression::IntNegation { minus_token, expr } => {
			let int = Type::Int(Default::default());
			let expr = expand_expr(state, *expr)?;
			expect_int(&expr.ty, expr.expr_span, minus_token.span)?;
			(format!("-({})", expr.code), span, int).into()
		},

		Expression::Arithmetic { lhs, op, rhs } => {
			let int = Type::Int(Default::default());
			let lhs = expand_expr(state, *lhs)?;
			expect_int(&lhs.ty, lhs.expr_span, op.span())?;
			let rhs = expand_expr(state, *rhs)?;
			expect_int(&rhs.ty, rhs.expr_span, op.span())?;
			(format!("{} {op} {}", lhs.code, rhs.code), span, int).into()
		},

		Expression::Comparison { lhs, op, rhs } => {
			let bool = Type::Bool(Default::default());
			let lhs = expand_expr(state, *lhs)?;
			expect_int(&lhs.ty, lhs.expr_span, op.span())?;
			let rhs = expand_expr(state, *rhs)?;
			expect_int(&rhs.ty, rhs.expr_span, op.span())?;
			(format!("{} {op} {}", lhs.code, rhs.code), span, bool).into()
		},

		Expression::Conjunction { lhs, op, rhs } => {
			let bool = Type::Bool(Default::default());
			let lhs = expand_expr(state, *lhs)?;
			expect_bool(&lhs.ty, lhs.expr_span, op.span())?;
			let rhs = expand_expr(state, *rhs)?;
			expect_bool(&rhs.ty, rhs.expr_span, op.span())?;
			(format!("{} {op} {}", lhs.code, rhs.code), span, bool).into()
		},

		Expression::TupleCtor { paren_token, elems } => {
			let mut ty_elems = Punctuated::new();
			let mut buf = "(".to_owned();
			for v in elems.into_iter() {
				let elem = expand_expr(state, v)?;
				buf += &elem.code;
				buf += ", ";
				ty_elems.push(elem.ty.into_owned());
			}
			buf += ")";
			let tuple = Type::Tuple(TypeTuple {
				paren_token,
				elems: ty_elems
			});
			(buf, span, tuple).into()
		},

		Expression::TupleIndex { expr, index, .. } => {
			let left = expand_expr(state, *expr)?;
			let idx = index.index as usize;
			expect_tuple_at_least(&left.ty, idx + 1, span, index.span())?;
			(
				format!("{}.{}", left.code, index.index),
				span,
				resolve_ty(left.ty.nth_ty(idx), state.generic_types()).into_owned()
			)
				.into()
		},

		Expression::RcCtor { span, expr, .. } => {
			let inner = expand_expr(state, *expr)?;
			let rc = Type::Rc(TypePredef {
				span,
				lt_token: Default::default(),
				inner_ty: Box::new(
					resolve_ty(&inner.ty, state.generic_types()).into_owned()
				),
				gt_token: Default::default()
			});
			(format!("::stdlib::Rc::new({})", inner.code), span, rc).into()
		},

		Expression::OptionCtor { span, expr: None } => {
			let option = Type::Option(TypePredef {
				span,
				lt_token: Default::default(),
				inner_ty: Box::new(Type::Any),
				gt_token: Default::default()
			});
			("::stdlib::Option::None", span, option).into()
		},

		Expression::OptionCtor {
			span,
			expr: Some((_, expr))
		} => {
			let inner = expand_expr(state, *expr)?;
			let option = Type::Option(TypePredef {
				span,
				lt_token: Default::default(),
				inner_ty: Box::new(
					resolve_ty(&inner.ty, state.generic_types()).into_owned()
				),
				gt_token: Default::default()
			});
			(
				format!("::stdlib::Option::Some({})", inner.code),
				span,
				option
			)
				.into()
		},

		Expression::Deref { star_token, expr } => {
			let rc = expand_expr(state, *expr)?;
			expect_rc(&rc.ty, rc.expr_span, star_token.span)?;
			(
				format!("::stdlib::Rc::into_inner({})", rc.code),
				span,
				rc.ty.inner_ty().to_owned()
			)
				.into()
		},

		Expression::FnCall {
			ident,
			generics,
			inputs,
			..
		} => {
			let fun = state.fun(&ident, &generics)?;

			let fun_inputs_len = fun.inputs_len();
			let inputs_len = inputs.len();
			if fun_inputs_len > inputs_len {
				return Err(Diagnostic::new(
					ident.span(),
					"Too few arguments",
					format!("but only {inputs_len} arguments were supplied")
				)
				.with_label(
					fun.span(),
					format!("This function takes {fun_inputs_len} arguments,")
				));
			}

			let mut buf = format!("{}(", fun.ident);
			for (input_ty, input_expr) in fun.inputs().zip(inputs.iter()) {
				let resolved_ty = resolve_ty(&input_ty, state.generic_types());
				buf += &expand_expr_with_type(state, &resolved_ty, input_expr.clone())?;
				buf += ", ";
			}
			buf += ")";

			(buf, span, fun.output().into_owned()).into()
		}
	};

	let expanded_ty = resolve_ty(&expanded.ty, state.generic_types());
	let expanded_ty: &Type = &expanded_ty;
	Ok(if matches!(expanded_ty, Type::Custom(_)) {
		let mut ty = expanded.ty.into_owned();
		let mut buf = expanded.code.into_owned();
		while let Type::Custom(custom) = ty {
			buf = format!("{}::into_inner({buf})", custom.ident);
			ty = resolve_tycustom(state, &custom)?.into_owned();
		}
		(buf, expanded.expr_span, ty).into()
	} else {
		expanded
	})
}

pub(super) fn expand_expr_with_type(
	state: &State<'_>,
	ty: &Type,
	expr: Expression
) -> Result<Cow<'static, str>, Diagnostic> {
	let ty = resolve_ty(ty, state.generic_types());
	let ty: &Type = &ty;

	// special case: custom outer type, requires wrapping in new
	if let Type::Custom(custom) = ty {
		let inner_ty = resolve_tycustom(state, custom)?;
		let ident = &custom.ident;
		let expr = expand_expr_with_type(state, &inner_ty, expr)?;
		return Ok(format!("{ident}::new({expr})").into());
	}

	Ok(match expr {
		// special case: possibly custom type(s) inside a term
		Expression::Term { expr, .. } => {
			let expr = expand_expr_with_type(state, ty, *expr)?;
			format!("({expr})").into()
		},

		// special case: possibly custom type(s) inside a tuple
		Expression::TupleCtor {
			paren_token, elems, ..
		} => {
			expect_tuple_exact(ty, elems.len(), paren_token.span, ty.span())?;
			let mut buf = "(".to_owned();
			for (n, v) in elems.into_iter().enumerate() {
				let elem = expand_expr_with_type(state, ty.nth_ty(n), v)?;
				buf += &elem;
				buf += ", ";
			}
			buf += ")";
			buf.into()
		},

		// special case: possibly custom type inside an Rc
		Expression::RcCtor { span, expr, .. } => {
			expect_rc_ctor(ty, span)?;
			let inner = expand_expr_with_type(state, ty.inner_ty(), *expr)?;
			format!("::stdlib::Rc::new({inner})").into()
		},

		// special case: possibly custom type inside a Some
		Expression::OptionCtor {
			span,
			expr: Some((_, expr))
		} => {
			expect_option_ctor(ty, span)?;
			let inner = expand_expr_with_type(state, ty.inner_ty(), *expr)?;
			format!("::stdlib::Option::Some({inner})").into()
		},

		// otherwise, just expand the expression and expect its type
		_ => {
			let expr = expand_expr(state, expr)?;
			expect_ty(ty, &expr.ty, expr.expr_span)?;
			expr.code
		}
	})
}
