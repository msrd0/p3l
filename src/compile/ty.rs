use super::State;
use crate::{ast::Type, diagnostic::Diagnostic};
use std::borrow::Cow;

pub(super) fn expand_type(
	state: &State<'_>,
	ty: &Type
) -> Result<Cow<'static, str>, Diagnostic> {
	Ok(match ty {
		Type::Bool(_) => "::stdlib::bool".into(),
		Type::Int(_) => "::stdlib::int".into(),

		Type::Tuple(tuple) => {
			let mut buf = "(".to_owned();
			for elem in &tuple.elems {
				buf += &expand_type(state, elem)?;
				buf += ", "
			}
			buf += ")";
			buf.into()
		},

		Type::Rc(rc) => {
			format!("::stdlib::Rc<{}>", expand_type(state, &rc.inner_ty)?).into()
		},

		Type::Option(option) => format!(
			"::stdlib::Option<{}>",
			expand_type(state, &option.inner_ty)?
		)
		.into(),

		Type::Custom(custom) => {
			if let Some(ty) = state.generic_types().get(&custom.ident) {
				if let Some(g) = custom.generics.as_ref() {
					return Err(Diagnostic::new(
						g.lt_token.span,
						"Invalid Type",
						"This generic type parameter cannot accept generics"
					));
				}
				expand_type(state, ty)?
			} else {
				let mut buf = format!("{}", custom.ident);
				if let Some(g) = &custom.generics {
					buf += "::<";
					for (i, ty) in g.params.iter().enumerate() {
						if i > 0 {
							buf += ", ";
						}
						buf += &expand_type(state, ty)?;
					}
					buf += ">";
				}
				buf.into()
			}
		},

		Type::Any => unimplemented!()
	})
}
