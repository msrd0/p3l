use super::{
	expr::expand_expr_with_type, resolve_ty::resolve_ty, ty::expand_type, State
};
use crate::{ast::Declaration, diagnostic::Diagnostic};

pub(super) fn expand_decl(
	state: &mut State<'_>,
	decl: Declaration
) -> Result<String, Diagnostic> {
	let expr = expand_expr_with_type(state, &decl.ty, decl.expr)?;
	let ident = decl.ident;
	let ty = expand_type(state, &decl.ty)?;
	let buf = format!("let mut {ident}: {ty} = {expr};");
	state.add_variable(
		ident,
		resolve_ty(&decl.ty, state.generic_types()).into_owned()
	);
	Ok(buf)
}
