#![allow(clippy::mixed_read_write_in_expression)]
#![warn(rust_2018_idioms, rustdoc::broken_intra_doc_links)]
#![deny(elided_lifetimes_in_paths)]
#![forbid(unsafe_code)]

use std::sync::atomic::{AtomicBool, Ordering};

pub static VERBOSE: AtomicBool = AtomicBool::new(false);

fn verbose() -> bool {
	VERBOSE.load(Ordering::Relaxed)
}

pub fn set_verbose(verbose: bool) {
	VERBOSE.store(verbose, Ordering::Relaxed)
}

macro_rules! bail {
	($span:expr => $msg:expr) => {
		return Err(syn::Error::new($span, format!($msg)))
	};
}

pub mod ast;
pub mod compile;
pub mod diagnostic;
