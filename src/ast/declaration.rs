use super::{Expression, Type};
use proc_macro2::Ident;
use syn::{
	parse::{Parse, ParseStream},
	Token
};

#[derive(Clone, Debug)]
pub struct Declaration {
	pub let_token: Token![let],
	pub ident: Ident,
	pub colon_token: Token![:],
	pub ty: Type,
	pub eq_token: Token![=],
	pub expr: Expression,
	pub semi_token: Token![;]
}

impl Parse for Declaration {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		Ok(Self {
			let_token: input.parse()?,
			ident: input.parse()?,
			colon_token: input.parse()?,
			ty: input.parse()?,
			eq_token: input.parse()?,
			expr: input.parse()?,
			semi_token: input.parse()?
		})
	}
}
