use super::{Block, Condition};
use syn::{
	parse::{Parse, ParseStream},
	Token
};

#[derive(Clone, Debug)]
pub struct While {
	pub while_token: Token![while],
	pub condition: Condition,
	pub loop_body: Block
}

impl Parse for While {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		Ok(Self {
			while_token: input.parse()?,
			condition: input.parse()?,
			loop_body: input.parse()?
		})
	}
}

#[derive(Clone, Debug)]
pub struct Continue {
	pub continue_token: Token![continue],
	pub semi_token: Token![;]
}

impl Parse for Continue {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		Ok(Self {
			continue_token: input.parse()?,
			semi_token: input.parse()?
		})
	}
}

#[derive(Clone, Debug)]
pub struct Break {
	pub break_token: Token![break],
	pub semi_token: Token![;]
}

impl Parse for Break {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		Ok(Self {
			break_token: input.parse()?,
			semi_token: input.parse()?
		})
	}
}
