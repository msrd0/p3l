use super::Expression;
use syn::{
	parse::{Parse, ParseStream},
	punctuated::Punctuated,
	spanned::Spanned,
	Member, Token
};

#[derive(Clone, Debug)]
pub struct Assignment {
	pub lhs: Punctuated<Member, Token![.]>,
	pub eq_token: Token![=],
	pub rhs: Expression,
	pub semi_token: Token![;]
}

impl Parse for Assignment {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		let lhs = Punctuated::parse_separated_nonempty(input)?;
		if lhs.trailing_punct() {
			bail!(lhs.span() => "Expected ident after fullstop");
		}
		Ok(Self {
			lhs,
			eq_token: input.parse()?,
			rhs: input.parse()?,
			semi_token: input.parse()?
		})
	}
}
