use super::{Generics, Type};
use proc_macro2::{Ident, Span};
use syn::{
	parse::{Parse, ParseStream},
	spanned::Spanned,
	Token
};

#[derive(Debug)]
pub struct TypeDef {
	pub type_token: Token![type],
	pub ident: Ident,
	pub generics: Option<Generics<Ident>>,
	pub eq_token: Token![=],
	pub ty: Type,
	pub semi_token: Token![;]
}

impl Parse for TypeDef {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		let type_token = input.parse()?;
		let ident = input.parse()?;

		let mut generics = None;
		if input.peek(Token![<]) {
			generics = Some(input.parse()?);
		}

		Ok(Self {
			type_token,
			ident,
			generics,
			eq_token: input.parse()?,
			ty: input.parse()?,
			semi_token: input.parse()?
		})
	}
}

impl Spanned for TypeDef {
	fn span(&self) -> Span {
		self.ident.span()
	}
}
