use super::Expression;
use syn::{
	parse::{Parse, ParseStream},
	Token
};

#[derive(Clone, Debug)]
pub struct Return {
	pub return_token: Token![return],
	pub expr: Option<Expression>,
	pub semi_token: Token![;]
}

impl Parse for Return {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		Ok(Self {
			return_token: input.parse()?,
			expr: (!input.peek(Token![;]))
				.then(|| input.parse())
				.transpose()?,
			semi_token: input.parse()?
		})
	}
}
