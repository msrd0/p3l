use super::Generics;
use proc_macro2::{Ident, Span};
use std::{
	borrow::Cow,
	fmt::{self, Display, Formatter, Write as _},
	hash::{Hash, Hasher}
};
use syn::{
	parse::{Parse, ParseStream},
	punctuated::Punctuated,
	spanned::Spanned,
	token::Paren,
	Token
};

#[derive(Clone, Debug)]
pub struct TypePrimitive {
	pub span: Span
}

impl Default for TypePrimitive {
	fn default() -> Self {
		Self {
			span: Span::call_site()
		}
	}
}

impl Hash for TypePrimitive {
	fn hash<H: Hasher>(&self, _state: &mut H) {}
}

impl Spanned for TypePrimitive {
	fn span(&self) -> Span {
		self.span
	}
}

#[derive(Clone, Debug, Default)]
pub struct TypeTuple {
	pub paren_token: Paren,
	pub elems: Punctuated<Type, Token![,]>
}

impl PartialEq for TypeTuple {
	fn eq(&self, other: &Self) -> bool {
		let len = self.elems.len();
		if len != other.elems.len() {
			return false;
		}
		for i in 0 .. len {
			if self.elems[i] != other.elems[i] {
				return false;
			}
		}
		true
	}
}

impl Hash for TypeTuple {
	fn hash<H: Hasher>(&self, state: &mut H) {
		// Punctuated implements Hash by hashing the inner Vec,
		// and Token![] types have an empty hash function
		self.elems.hash(state);
	}
}

impl Spanned for TypeTuple {
	fn span(&self) -> Span {
		self.paren_token.span
	}
}

#[derive(Clone, Debug)]
pub struct TypePredef {
	pub span: Span,
	pub lt_token: Token![<],
	pub inner_ty: Box<Type>,
	pub gt_token: Token![>]
}

impl PartialEq for TypePredef {
	fn eq(&self, other: &Self) -> bool {
		self.inner_ty == other.inner_ty
	}
}

impl Hash for TypePredef {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.inner_ty.hash(state);
	}
}

impl Spanned for TypePredef {
	fn span(&self) -> Span {
		self.span
	}
}

#[derive(Clone, Debug)]
pub struct TypeCustom {
	pub ident: Ident,
	pub generics: Option<Generics<Type>>
}

impl PartialEq for TypeCustom {
	fn eq(&self, other: &Self) -> bool {
		self.ident == other.ident && self.generics == other.generics
	}
}

impl Hash for TypeCustom {
	fn hash<H: Hasher>(&self, state: &mut H) {
		// Ident implements Hash by hashing its string representation
		self.ident.hash(state);
		self.generics.hash(state);
	}
}

impl Spanned for TypeCustom {
	fn span(&self) -> Span {
		self.ident.span()
	}
}

#[derive(Clone, Debug, Hash)]
pub enum Type {
	Bool(TypePrimitive),
	Int(TypePrimitive),
	Tuple(TypeTuple),
	Rc(TypePredef),
	Option(TypePredef),
	Custom(TypeCustom),

	/// This type is only used when expanding expressions, it is
	/// never parsed from input.
	Any
}

impl Display for Type {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Bool(_) => f.write_str("bool"),
			Self::Int(_) => f.write_str("int"),
			Self::Tuple(tuple) => {
				f.write_str("(")?;
				for (i, ty) in tuple.elems.iter().enumerate() {
					if i > 0 {
						f.write_str(" ")?;
					}
					write!(f, "{ty},")?;
				}
				f.write_str(")")
			},
			Self::Rc(rc) => write!(f, "Rc<{}>", rc.inner_ty),
			Self::Option(opt) => write!(f, "Option<{}>", opt.inner_ty),
			Self::Custom(custom) => {
				write!(f, "{}", custom.ident)?;
				if let Some(g) = &custom.generics {
					write!(f, "{g}")?;
				}
				Ok(())
			},
			Self::Any => f.write_str("_")
		}
	}
}

impl Type {
	/// Returns a mangled string representation of this type. Panics
	/// if this type contains `_`.
	pub fn mangled(&self) -> Cow<'static, str> {
		match self {
			Self::Bool(_) => "B".into(),
			Self::Int(_) => "I".into(),
			Self::Tuple(tuple) => {
				let mut buf = format!("T{}", tuple.elems.len());
				for ty in tuple.elems.iter() {
					write!(buf, "_{}", ty.mangled()).unwrap();
				}
				buf.into()
			},
			Self::Rc(rc) => format!("R{}", rc.inner_ty.mangled()).into(),
			Self::Option(opt) => format!("O{}", opt.inner_ty.mangled()).into(),
			Self::Custom(custom) => {
				let ident = custom.ident.to_string();
				let mut buf = format!("C{}_{}", ident.len(), ident);
				if let Some(g) = &custom.generics {
					write!(buf, "_G{}", g.params.len()).unwrap();
					for param in g.params.iter() {
						write!(buf, "_{}", param.mangled()).unwrap()
					}
				}
				buf.into()
			},
			Self::Any => panic!("Cannot mangle type containing _")
		}
	}
}

impl From<Type> for Cow<'_, Type> {
	fn from(this: Type) -> Self {
		Cow::Owned(this)
	}
}

impl<'a> From<&'a Type> for Cow<'a, Type> {
	fn from(this: &'a Type) -> Self {
		Cow::Borrowed(this)
	}
}

impl PartialEq for Type {
	fn eq(&self, other: &Self) -> bool {
		match (self, other) {
			(Self::Bool(_), Self::Bool(_)) | (Self::Int(_), Self::Int(_)) => true,
			(Self::Tuple(this), Self::Tuple(other)) => this == other,
			(Self::Rc(this), Self::Rc(other)) => this == other,
			(Self::Option(this), Self::Option(other)) => this == other,
			(Self::Custom(this), Self::Custom(other)) => this == other,
			(Self::Any, _) | (_, Self::Any) => true,
			_ => false
		}
	}
}

impl Eq for Type {}

impl Type {
	/// Return the inner type for `Rc` and `Option`. Panics otherwise.
	pub fn inner_ty(&self) -> &Type {
		match self {
			Self::Rc(this) | Self::Option(this) => &this.inner_ty,
			_ => panic!("This type does not have an inner type")
		}
	}

	/// Return the inner type for `Rc` and `Option`. Panics otherwise.
	pub fn into_inner_ty(self) -> Type {
		match self {
			Self::Rc(this) | Self::Option(this) => *this.inner_ty,
			_ => panic!("This type does not have an inner type")
		}
	}

	/// Return the n-th inner type for tuples. Panics if self is not a tuple or
	/// if n is out of bounds.
	pub fn nth_ty(&self, n: usize) -> &Type {
		match self {
			Self::Tuple(tuple) => tuple.elems.iter().nth(n).expect("out of bounds"),
			_ => panic!("This type is not a tuple")
		}
	}
}

impl Parse for Type {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		if input.peek(Paren) {
			let content;
			let paren_token = syn::parenthesized!(content in input);
			let elems = Punctuated::parse_terminated(&content)?;
			return Ok(if elems.len() == 1 && !elems.trailing_punct() {
				elems.into_iter().next().unwrap()
			} else {
				Self::Tuple(TypeTuple { paren_token, elems })
			});
		}

		let ident: Ident = input.parse()?;
		if ident == "bool" {
			return Ok(Self::Bool(TypePrimitive { span: ident.span() }));
		} else if ident == "int" {
			return Ok(Self::Int(TypePrimitive { span: ident.span() }));
		}

		if !input.peek(Token![<]) {
			if ident == "Rc" || ident == "Option" {
				bail!(ident.span() => "Missing generic type parameter")
			}
			return Ok(Self::Custom(TypeCustom {
				ident,
				generics: None
			}));
		}

		let generics: Generics<Type> = input.parse()?;

		if ident == "Rc" || ident == "Option" {
			let mut iter = generics.params.into_iter();
			let inner_ty = Box::new(iter.next().unwrap());
			if let Some(p) = iter.next() {
				bail!(p.span() => "Too many generic type paramaters");
			}
			let predef = TypePredef {
				span: ident.span(),
				lt_token: generics.lt_token,
				inner_ty,
				gt_token: generics.gt_token
			};
			return Ok(if ident == "Rc" {
				Self::Rc(predef)
			} else {
				Self::Option(predef)
			});
		}

		Ok(Self::Custom(TypeCustom {
			ident,
			generics: Some(generics)
		}))
	}
}

impl Spanned for Type {
	fn span(&self) -> Span {
		match self {
			Self::Bool(this) | Self::Int(this) => this.span(),
			Self::Tuple(this) => this.span(),
			Self::Rc(this) | Self::Option(this) => this.span(),
			Self::Custom(this) => this.span(),
			Self::Any => Span::call_site()
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn parse_bool() {
		let ty: Type = syn::parse_str("bool").unwrap();
		assert!(matches!(ty, Type::Bool(_)));
	}

	#[test]
	fn parse_int() {
		let ty: Type = syn::parse_str("int").unwrap();
		assert!(matches!(ty, Type::Int(_)));
	}

	#[test]
	fn parse_unit() {
		let ty: Type = syn::parse_str("()").unwrap();
		assert!(matches!(ty, Type::Tuple(_)));
		match ty {
			Type::Tuple(tuple) => assert!(tuple.elems.is_empty()),
			_ => unreachable!()
		}
	}

	#[test]
	fn parse_paren_int() {
		let ty: Type = syn::parse_str("(int)").unwrap();
		assert!(matches!(ty, Type::Int(_)));
	}

	#[test]
	fn parse_tuple_1() {
		let ty: Type = syn::parse_str("(int,)").unwrap();
		assert!(matches!(ty, Type::Tuple(_)));
		match ty {
			Type::Tuple(tuple) => {
				let mut iter = tuple.elems.into_iter();
				assert!(matches!(iter.next(), Some(Type::Int(_))));
				assert!(iter.next().is_none());
			},
			_ => unreachable!()
		}
	}

	#[test]
	fn parse_tuple_2() {
		let ty: Type = syn::parse_str("(int, Option<int>)").unwrap();
		assert!(matches!(ty, Type::Tuple(_)));
		match ty {
			Type::Tuple(tuple) => {
				let mut iter = tuple.elems.into_iter();
				assert!(matches!(iter.next(), Some(Type::Int(_))));
				assert!(matches!(iter.next(), Some(Type::Option(_))));
				assert!(iter.next().is_none());
			},
			_ => unreachable!()
		}
	}

	#[test]
	fn parse_tuple_2_trailing() {
		let ty: Type = syn::parse_str("(int, Option<int>,)").unwrap();
		assert!(matches!(ty, Type::Tuple(_)));
		match ty {
			Type::Tuple(tuple) => {
				let mut iter = tuple.elems.into_iter();
				assert!(matches!(iter.next(), Some(Type::Int(_))));
				assert!(matches!(iter.next(), Some(Type::Option(_))));
				assert!(iter.next().is_none());
			},
			_ => unreachable!()
		}
	}

	#[test]
	fn parse_rc() {
		let ty: Type = syn::parse_str("Rc<int>").unwrap();
		assert!(matches!(ty, Type::Rc(_)));
		match ty {
			Type::Rc(rc) => assert!(matches!(*rc.inner_ty, Type::Int(_))),
			_ => unreachable!()
		}
	}

	#[test]
	fn parse_option() {
		let ty: Type = syn::parse_str("Option<int>").unwrap();
		assert!(matches!(ty, Type::Option(_)));
		match ty {
			Type::Option(option) => assert!(matches!(*option.inner_ty, Type::Int(_))),
			_ => unreachable!()
		}
	}

	#[test]
	fn parse_custom_without_generics() {
		let ty: Type = syn::parse_str("Foo").unwrap();
		assert!(matches!(ty, Type::Custom(_)));
		match ty {
			Type::Custom(custom) => {
				assert_eq!(custom.ident, "Foo");
				assert!(custom.generics.is_none());
			},
			_ => unreachable!()
		}
	}

	#[test]
	fn parse_custom_with_generics() {
		let ty: Type = syn::parse_str("Foo<int>").unwrap();
		assert!(matches!(ty, Type::Custom(_)));
		match ty {
			Type::Custom(custom) => {
				assert_eq!(custom.ident, "Foo");
				assert!(custom.generics.is_some());
				match custom.generics {
					Some(generics) => {
						let mut iter = generics.params.into_iter();
						assert!(matches!(iter.next(), Some(Type::Int(_))));
						assert!(iter.next().is_none());
					},
					None => unreachable!()
				}
			},
			_ => unreachable!()
		}
	}
}
