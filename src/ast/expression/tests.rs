use super::*;

#[track_caller]
fn parse_expr(input: &str) -> Expression {
	println!("Input: {input}");
	syn::parse_str(input).expect("Failed to parse input")
}

#[track_caller]
fn assert_ident(expr: Expression, name: &str) {
	match expr {
		Expression::Ident(ident) if ident == name => {},
		expr => panic!("Expected `{name}`, found `{expr}`")
	}
}

#[track_caller]
fn assert_bool(expr: Expression, value: bool) {
	match expr {
		Expression::Bool(lit) if lit.value() == value => {},
		expr => panic!("Expected `{value}`, found `{expr}`")
	}
}

#[track_caller]
fn assert_int(expr: Expression, value: isize) {
	match expr {
		Expression::Literal(lit)
			if lit
				.base10_parse::<isize>()
				.map(|lit| lit == value)
				.unwrap_or(false) => {},
		expr => panic!("Expected `{value}`, found `{expr}`")
	}
}

#[track_caller]
fn assert_bool_negation(expr: Expression) -> Expression {
	match expr {
		Expression::BoolNegation { expr, .. } => *expr,
		expr => {
			panic!("Expected `!{{ .. }}`, found `{expr}`")
		}
	}
}

#[track_caller]
fn assert_int_negation(expr: Expression) -> Expression {
	match expr {
		Expression::IntNegation { expr, .. } => *expr,
		expr => {
			panic!("Expected `-{{ .. }}`, found `{expr}`")
		}
	}
}

#[track_caller]
fn assert_arithmetic(expr: Expression, op_char: char) -> (Expression, Expression) {
	match expr {
		Expression::Arithmetic { lhs, op, rhs } if op.char() == op_char => (*lhs, *rhs),
		expr => panic!("Expected `{{ .. }} {op_char} {{ .. }}`, found `{expr}`")
	}
}

#[track_caller]
fn assert_comparison(expr: Expression, op_str: &str) -> (Expression, Expression) {
	match expr {
		Expression::Comparison { lhs, op, rhs } if op.str() == op_str => (*lhs, *rhs),
		expr => panic!("Expected `{{ .. }} {op_str} {{ .. }}`, found `{expr}`")
	}
}

#[track_caller]
fn assert_conjunction(expr: Expression, op_str: &str) -> (Expression, Expression) {
	match expr {
		Expression::Conjunction { lhs, op, rhs } if op.str() == op_str => (*lhs, *rhs),
		expr => panic!("Expected `{{ .. }} {op_str} {{ .. }}`, found `{expr}`")
	}
}

#[track_caller]
fn assert_tuple_index(expr: Expression, idx: u32) -> Expression {
	match expr {
		Expression::TupleIndex { expr, index, .. } if index.index == idx => *expr,
		expr => panic!("Expected `{{ .. }}.{idx}`, found `{expr}`")
	}
}

#[track_caller]
fn assert_deref(expr: Expression) -> Expression {
	match expr {
		Expression::Deref { expr, .. } => *expr,
		expr => panic!("*{{ .. }}`, found `{expr}`")
	}
}

#[test]
fn parse_true() {
	let expr = parse_expr("true");
	assert_bool(expr, true);
}

#[test]
fn parse_false() {
	let expr = parse_expr("false");
	assert_bool(expr, false);
}

#[test]
fn parse_zero() {
	let expr = parse_expr("0");
	assert_int(expr, 0);
}

#[test]
fn parse_ten() {
	let expr = parse_expr("10");
	assert_int(expr, 10);
}

#[test]
fn parse_minus_ten() {
	let expr = parse_expr("-10");
	assert_int(expr, -10);
}

#[test]
fn parse_not_true() {
	let expr = parse_expr("!true");
	let expr = assert_bool_negation(expr);
	assert_bool(expr, true);
}

#[test]
fn parse_minus_minus_ten() {
	let expr = parse_expr("--10");
	let expr = assert_int_negation(expr);
	assert_int(expr, -10);
}

#[test]
fn parse_one_plus_two() {
	let expr = parse_expr("1 + 2");
	let (lhs, rhs) = assert_arithmetic(expr, '+');
	assert_int(lhs, 1);
	assert_int(rhs, 2);
}

#[test]
fn parse_one_plus_two_times_three() {
	let expr = parse_expr("1 + 2 * 3");
	let (lhs, rhs) = assert_arithmetic(expr, '+');
	assert_int(lhs, 1);
	let (lhs, rhs) = assert_arithmetic(rhs, '*');
	assert_int(lhs, 2);
	assert_int(rhs, 3);
}

#[test]
fn parse_one_times_two_plus_three_times_four() {
	let expr = parse_expr("1 * 2 + 3 * 4");
	let (lhs, rhs) = assert_arithmetic(expr, '+');
	{
		let (lhs, rhs) = assert_arithmetic(lhs, '*');
		assert_int(lhs, 1);
		assert_int(rhs, 2);
	}
	{
		let (lhs, rhs) = assert_arithmetic(rhs, '*');
		assert_int(lhs, 3);
		assert_int(rhs, 4);
	}
}

#[test]
fn parse_one_times_two_eq_three_plus_four() {
	let expr = parse_expr("1 * 2 == 3 + 4");
	let (lhs, rhs) = assert_comparison(expr, "==");
	{
		let (lhs, rhs) = assert_arithmetic(lhs, '*');
		assert_int(lhs, 1);
		assert_int(rhs, 2);
	}
	{
		let (lhs, rhs) = assert_arithmetic(rhs, '+');
		assert_int(lhs, 3);
		assert_int(rhs, 4);
	}
}

#[test]
fn parse_true_and_one_plus_two_eq_three_times_four() {
	let expr = parse_expr("true && 1 + 2 == 3 * 4");
	let (lhs, rhs) = assert_conjunction(expr, "&&");
	assert_bool(lhs, true);
	let (lhs, rhs) = assert_comparison(rhs, "==");
	{
		let (lhs, rhs) = assert_arithmetic(lhs, '+');
		assert_int(lhs, 1);
		assert_int(rhs, 2);
	}
	{
		let (lhs, rhs) = assert_arithmetic(rhs, '*');
		assert_int(lhs, 3);
		assert_int(rhs, 4);
	}
}

#[test]
fn parse_not_true_and_false() {
	let expr = parse_expr("!true && false");
	let (lhs, rhs) = assert_conjunction(expr, "&&");
	let lhs = assert_bool_negation(lhs);
	assert_bool(lhs, true);
	assert_bool(rhs, false);
}

#[test]
fn parse_not_ident_0_and_false() {
	let expr = parse_expr("!foo.0 && false");
	let (lhs, rhs) = assert_conjunction(expr, "&&");
	let lhs = assert_bool_negation(lhs);
	let lhs = assert_tuple_index(lhs, 0);
	assert_ident(lhs, "foo");
	assert_bool(rhs, false);
}

#[test]
fn parse_deref_ident() {
	let expr = parse_expr("*foo");
	let expr = assert_deref(expr);
	assert_ident(expr, "foo");
}

#[test]
fn parse_deref_ident_0() {
	let expr = parse_expr("*foo.0");
	let expr = assert_deref(expr);
	let expr = assert_tuple_index(expr, 0);
	assert_ident(expr, "foo");
}

#[test]
fn parse_deref_ident_eq_one() {
	let expr = parse_expr("*foo == 1");
	let (lhs, rhs) = assert_comparison(expr, "==");
	let lhs = assert_deref(lhs);
	assert_ident(lhs, "foo");
	assert_int(rhs, 1);
}

#[test]
fn parse_zero_eq_foo_0() {
	let expr = parse_expr("0 == foo.0");
	let (lhs, rhs) = assert_comparison(expr, "==");
	assert_int(lhs, 0);
	let rhs = assert_tuple_index(rhs, 0);
	assert_ident(rhs, "foo");
}
