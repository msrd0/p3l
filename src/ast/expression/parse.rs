use super::{ArithmeticOp, ComparisonOp, ConjunctionOp, Expression};
use proc_macro2::Ident;
use syn::{
	parse::{Parse, ParseStream},
	punctuated::Punctuated,
	token::Paren,
	Index, LitBool, LitInt, Token
};

macro_rules! parse_next_op {
	($left:ident, $input:ident,Self:: $fn:ident, $op:ident:: $variant:ident) => {
		(
			Self::$fn(
				$left,
				$op::$variant($input.parse()?),
				Self::parse_first($input)?
			),
			false
		)
	};
}

macro_rules! reorder_expr {
	($lhs:ident, $op:ident, $rhs:ident => Self::$new_variant:ident {
		$(Self::$variant:ident {
			$lhs_lhs:ident,
			$lhs_op:ident,
			$lhs_rhs:ident
		} $(if $condition:expr)?),*
	}) => {{
		// This method will not be called when there are not variants in the macro
		// Also, the variables in the pattern can only be used by the if condition
		#[allow(dead_code, unused_variables)]
		fn filter(expr: &Expression) -> bool {
			match expr {
				$(Expression::$variant {
					lhs: $lhs_lhs,
					op: $lhs_op,
					rhs: $lhs_rhs
				} $(if $condition)? => true,)*
				_ => false
			}
		}

		match $lhs {
			$(Self::$variant {
				lhs: $lhs_lhs,
				op: $lhs_op,
				rhs: $lhs_rhs
			} $(if $condition)? => {
				Self::$variant {
					lhs: $lhs_lhs,
					op: $lhs_op,
					rhs: $lhs_rhs
				}.replace_rightmost(filter, |lhs_rhs| {
					Self::$new_variant {
                        lhs: Box::new(lhs_rhs),
                        op: $op,
                        rhs: Box::new($rhs)
                    }
				})
			},)*
			lhs => Self::$new_variant {
				lhs: Box::new(lhs),
				op: $op,
				rhs: Box::new($rhs)
			}
		}
	}};
}

impl Expression {
	/// Parse the first expression from the input. Might not be the full
	/// expression.
	fn parse_first(input: ParseStream<'_>) -> syn::Result<Self> {
		if input.peek(Paren) {
			let content;
			let paren_token = syn::parenthesized!(content in input);

			let elems = Punctuated::parse_terminated(&content)?;
			return Ok(if elems.len() == 1 && !elems.trailing_punct() {
				// parenthezised expression, not a tuple
				Self::Term {
					paren_token,
					expr: Box::new(elems.into_iter().next().unwrap())
				}
			} else {
				// tuple
				Self::TupleCtor { paren_token, elems }
			});
		}

		if input.peek(LitBool) {
			return Ok(Self::Bool(input.parse()?));
		}

		if input.peek(LitInt) {
			return Ok(Self::Literal(input.parse()?));
		}

		if input.peek(Token![!]) {
			return Ok(Self::BoolNegation {
				not_token: input.parse()?,
				expr: Box::new(Self::parse_first(input)?)
			});
		}

		if input.peek(Token![-]) {
			return Ok(Self::IntNegation {
				minus_token: input.parse()?,
				expr: Box::new(Self::parse_first(input)?)
			});
		}

		if input.peek(Token![*]) {
			return Ok(Self::Deref {
				star_token: input.parse()?,
				expr: Box::new(Self::parse_first(input)?)
			});
		}

		let ident: Ident = input.parse()?;

		if input.peek(Token![.]) {
			let mut left = Self::Ident(ident);
			while input.peek(Token![.]) {
				left = Self::tuple_index(left, input.parse()?, input.parse()?);
			}
			return Ok(left);
		}

		Ok(if ident == "Rc" {
			let content;
			let paren_token = syn::parenthesized!(content in input);
			Self::RcCtor {
				span: ident.span(),
				paren_token,
				expr: Box::new(content.parse()?)
			}
		} else if ident == "Some" {
			let content;
			let paren_token = syn::parenthesized!(content in input);
			Self::OptionCtor {
				span: ident.span(),
				expr: Some((paren_token, Box::new(content.parse()?)))
			}
		} else if ident == "None" {
			Self::OptionCtor {
				span: ident.span(),
				expr: None
			}
		} else if input.peek(Paren) {
			let content;
			let paren_token = syn::parenthesized!(content in input);
			let inputs = Punctuated::parse_terminated(&content)?;
			Self::FnCall {
				ident,
				generics: None,
				paren_token,
				inputs
			}
		} else if input.peek(Token![::]) {
			let _turbofish: Token![::] = input.parse()?;
			let generics = Some(input.parse()?);
			let content;
			let paren_token = syn::parenthesized!(content in input);
			let inputs = Punctuated::parse_terminated(&content)?;
			Self::FnCall {
				ident,
				generics,
				paren_token,
				inputs
			}
		} else {
			Self::Ident(ident)
		})
	}

	// this is the strongest binding binary operator, and since all unary
	// operators are left of their argument we don't need to take them
	// into account
	fn mul_or_div_op(lhs: Self, op: ArithmeticOp, rhs: Self) -> Self {
		debug_assert!(op.is_mul_or_div_op());
		reorder_expr!(lhs, op, rhs => Self::Arithmetic {
			// ¦a + b¦ * c = a + (b * c)
			Self::Arithmetic { lhs_lhs, lhs_op, lhs_rhs } if lhs_op.is_add_or_sub_op(),

			// ¦a == b¦ * c = a == (b * c)
			Self::Comparison { lhs_lhs, lhs_op, lhs_rhs },

			// ¦a && b¦ * c = a && (b * c)
			// (this is essential to allow comparison operators to be parsed next)
			Self::Conjunction { lhs_lhs, lhs_op, lhs_rhs }
		})
	}

	// this is the 2nd strongest binding binary operator, after mul or div and
	// before any of the boolean operators
	fn add_or_sub_op(lhs: Self, op: ArithmeticOp, rhs: Self) -> Self {
		debug_assert!(op.is_add_or_sub_op());
		reorder_expr!(lhs, op, rhs => Self::Arithmetic {
			// ¦a == b¦ * c = a == (b * c)
			Self::Comparison { lhs_lhs, lhs_op, lhs_rhs },

			// ¦a && b¦ * c = a && (b * c)
			// (this is essential to allow comparison operators to be parsed next)
			Self::Conjunction { lhs_lhs, lhs_op, lhs_rhs }
		})
	}

	// this is the 3rd strongest binding binary operator, after both arithmetic
	// operators
	fn comparison_op(lhs: Self, op: ComparisonOp, rhs: Self) -> Self {
		reorder_expr!(lhs, op, rhs => Self::Comparison {
			// ¦a && b¦ == c = a && (b == c)
			Self::Conjunction { lhs_lhs, lhs_op, lhs_rhs }
		})
	}

	// this is the least strongest binding binary operator
	fn conjunction_op(lhs: Self, op: ConjunctionOp, rhs: Self) -> Self {
		reorder_expr!(lhs, op, rhs => Self::Conjunction {})
	}

	// tuple indexing binds stronger than unary operators
	fn tuple_index(left: Self, dot_token: Token![.], index: Index) -> Self {
		match left {
			Self::BoolNegation { not_token, expr } => Self::BoolNegation {
				not_token,
				expr: Box::new(Self::tuple_index(*expr, dot_token, index))
			},
			Self::IntNegation { minus_token, expr } => Self::IntNegation {
				minus_token,
				expr: Box::new(Self::tuple_index(*expr, dot_token, index))
			},
			Self::Deref { star_token, expr } => Self::Deref {
				star_token,
				expr: Box::new(Self::tuple_index(*expr, dot_token, index))
			},
			left => Self::TupleIndex {
				expr: Box::new(left),
				dot_token,
				index
			}
		}
	}

	fn parse_next(left: Self, input: ParseStream<'_>) -> syn::Result<(Self, bool)> {
		if input.is_empty() {
			return Ok((left, true));
		}

		Ok(if input.peek(Token![.]) {
			(
				Self::tuple_index(left, input.parse()?, input.parse()?),
				false
			)
		} else if input.peek(Token![+]) {
			parse_next_op!(left, input, Self::add_or_sub_op, ArithmeticOp::Add)
		} else if input.peek(Token![-]) {
			parse_next_op!(left, input, Self::add_or_sub_op, ArithmeticOp::Sub)
		} else if input.peek(Token![*]) {
			parse_next_op!(left, input, Self::mul_or_div_op, ArithmeticOp::Mul)
		} else if input.peek(Token![/]) {
			parse_next_op!(left, input, Self::mul_or_div_op, ArithmeticOp::Div)
		} else if input.peek(Token![==]) {
			parse_next_op!(left, input, Self::comparison_op, ComparisonOp::Eq)
		} else if input.peek(Token![!=]) {
			parse_next_op!(left, input, Self::comparison_op, ComparisonOp::Neq)
		} else if input.peek(Token![<=]) {
			parse_next_op!(left, input, Self::comparison_op, ComparisonOp::Le)
		} else if input.peek(Token![<]) {
			parse_next_op!(left, input, Self::comparison_op, ComparisonOp::Lt)
		} else if input.peek(Token![>=]) {
			parse_next_op!(left, input, Self::comparison_op, ComparisonOp::Ge)
		} else if input.peek(Token![>]) {
			parse_next_op!(left, input, Self::comparison_op, ComparisonOp::Gt)
		} else if input.peek(Token![&&]) {
			parse_next_op!(left, input, Self::conjunction_op, ConjunctionOp::And)
		} else if input.peek(Token![||]) {
			parse_next_op!(left, input, Self::conjunction_op, ConjunctionOp::Or)
		} else {
			(left, true)
		})
	}
}

impl Parse for Expression {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		let mut expr = Self::parse_first(input)?;
		loop {
			let last;
			(expr, last) = Self::parse_next(expr, input)?;
			if last {
				break;
			}
		}
		Ok(expr)
	}
}
