use crate::verbose;
use proc_macro2::Ident;
use syn::{
	parse::{Parse, ParseStream},
	token::{Brace, Bracket},
	LitFloat, Token
};

mod assignment;
mod conditional;
mod declaration;
mod expression;
mod function;
mod generics;
mod input_output;
mod ret;
mod ty;
mod typedef;
mod while_loop;

pub use assignment::Assignment;
pub use conditional::{Condition, ElseBranch, If};
pub use declaration::Declaration;
pub use expression::{ArithmeticOp, ComparisonOp, Expression};
pub use function::{Fn, FnArg};
pub use generics::Generics;
pub use input_output::{Input, Output};
pub use ret::Return;
pub use ty::{Type, TypeCustom, TypePredef, TypePrimitive, TypeTuple};
pub use typedef::TypeDef;
pub use while_loop::{Break, Continue, While};

#[derive(Debug)]
pub struct Program {
	pub types: Vec<TypeDef>,
	pub functions: Vec<Fn>,
	pub inputs: Vec<Input>,
	pub stmts: Vec<Statement>,
	pub outputs: Vec<Output>
}

fn peek_macro(input: ParseStream<'_>, macro_ident: &str) -> bool {
	let fork = input.fork();
	let next = fork.parse::<Ident>();
	let next2 = fork.parse::<Token![!]>();
	matches!((next, next2), (Ok(ident), Ok(_)) if ident == macro_ident)
}

impl Parse for Program {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		let mut types = Vec::new();
		while input.peek(Token![type]) {
			if verbose() {
				eprint!("[ast] Parsing typedef ...");
			}
			types.push(input.parse()?);
			if verbose() {
				eprintln!("done");
			}
		}

		let mut functions = Vec::new();
		while input.peek(Token![fn]) {
			if verbose() {
				eprint!("[ast] Parsing function ...");
			}
			functions.push(input.parse()?);
			if verbose() {
				eprintln!("done");
			}
		}

		let mut inputs = Vec::new();
		while peek_macro(input, "input") {
			if verbose() {
				eprint!("[ast] Parsing input!() ...");
			}
			inputs.push(input.parse()?);
			if verbose() {
				eprintln!("done");
			}
		}

		let mut stmts = Vec::new();
		while !input.is_empty() && !peek_macro(input, "output") {
			if verbose() {
				eprint!("[ast] Parsing statement ...");
			}
			stmts.push(input.parse()?);
			if verbose() {
				eprintln!("done");
			}
		}

		let mut outputs = Vec::new();
		while !input.is_empty() {
			if verbose() {
				eprint!("[ast] Parsing output!() ...");
			}
			outputs.push(input.parse()?);
			if verbose() {
				eprintln!("done");
			}
		}

		if verbose() {
			eprintln!("[ast] Done parsing");
		}
		Ok(Self {
			types,
			functions,
			inputs,
			stmts,
			outputs
		})
	}
}

#[derive(Clone, Debug)]
pub struct Block {
	pub stmts: Vec<Statement>
}

impl Parse for Block {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		let content;
		syn::braced!(content in input);
		let mut stmts = Vec::new();
		while !content.is_empty() {
			stmts.push(content.parse()?);
		}
		Ok(Self { stmts })
	}
}

#[derive(Clone, Debug)]
pub enum Statement {
	Decl(Declaration),
	Assign(Assignment),
	If(If),
	CoinFlip { head: Block, prob: f32, tail: Block },
	While(While),
	Block(Block),
	Return(Return),
	Continue(Continue),
	Break(Break)
}

impl Parse for Statement {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		Ok(if input.peek(Token![let]) {
			Self::Decl(input.parse()?)
		} else if input.peek(Token![if]) {
			Self::If(input.parse()?)
		} else if input.peek(Token![while]) {
			Self::While(input.parse()?)
		} else if input.peek(Token![return]) {
			Self::Return(input.parse()?)
		} else if input.peek(Token![continue]) {
			Self::Continue(input.parse()?)
		} else if input.peek(Token![break]) {
			Self::Break(input.parse()?)
		} else if input.peek(Brace) {
			let block = input.parse()?;
			if input.peek(Bracket) {
				let content;
				syn::bracketed!(content in input);
				let prob: LitFloat = content.parse()?;
				let tail = input.parse()?;
				Self::CoinFlip {
					head: block,
					prob: prob.base10_parse()?,
					tail
				}
			} else {
				Self::Block(block)
			}
		} else if input.peek(syn::Ident) {
			Self::Assign(input.parse()?)
		} else {
			bail!(input.span() => "Unexpected token");
		})
	}
}
