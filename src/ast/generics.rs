use std::{
	fmt::{self, Display, Formatter},
	hash::{Hash, Hasher}
};
use syn::{
	parse::{Parse, ParseStream},
	punctuated::Punctuated,
	token::{Comma, Gt, Lt}
};

#[derive(Clone, Debug)]
pub struct Generics<T> {
	pub lt_token: Lt,
	pub params: Punctuated<T, Comma>,
	pub gt_token: Gt
}

impl<T: Display> Display for Generics<T> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.write_str("<")?;
		for (i, param) in self.params.iter().enumerate() {
			if i > 0 {
				f.write_str(", ")?;
			}
			write!(f, "{param}")?;
		}
		f.write_str(">")
	}
}

impl<T: PartialEq> PartialEq for Generics<T> {
	fn eq(&self, other: &Self) -> bool {
		self.params == other.params
	}
}

impl<T: Eq> Eq for Generics<T> {}

impl<T: Hash> Hash for Generics<T> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.params.hash(state);
	}
}

impl<T: Parse> Parse for Generics<T> {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		Ok(Self {
			lt_token: input.parse()?,
			params: Punctuated::parse_separated_nonempty(input)?,
			gt_token: input.parse()?
		})
	}
}
