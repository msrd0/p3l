use proc_macro2::{Ident, Span};
use syn::{
	parse::{Parse, ParseStream},
	punctuated::Punctuated,
	token::{Bang, Paren, Semi},
	Token
};

#[derive(Debug)]
pub struct InputOutputMacro<C> {
	pub span: Span,
	pub not_token: Bang,
	pub paren_token: Paren,
	pub content: C,
	pub semi_token: Semi
}

#[derive(Debug)]
pub struct Input {
	pub span: Span,
	pub not_token: Token![!],
	pub paren_token: Paren,
	pub ident: Ident,
	pub semi_token: Token![;]
}

impl From<InputOutputMacro<Ident>> for Input {
	fn from(this: InputOutputMacro<Ident>) -> Self {
		Self {
			span: this.span,
			not_token: this.not_token,
			paren_token: this.paren_token,
			ident: this.content,
			semi_token: this.semi_token
		}
	}
}

#[derive(Debug)]
pub struct Output {
	pub span: Span,
	pub not_token: Token![!],
	pub paren_token: Paren,
	pub idents: Punctuated<Ident, Token![,]>,
	pub semi_token: Token![;]
}

impl From<InputOutputMacro<Punctuated<Ident, Token![,]>>> for Output {
	fn from(this: InputOutputMacro<Punctuated<Ident, Token![,]>>) -> Self {
		Self {
			span: this.span,
			not_token: this.not_token,
			paren_token: this.paren_token,
			idents: this.content,
			semi_token: this.semi_token
		}
	}
}

fn parse<I, F>(
	input: ParseStream<'_>,
	macro_ident: &str,
	content_parser: F
) -> syn::Result<InputOutputMacro<I>>
where
	F: FnOnce(ParseStream<'_>) -> syn::Result<I>
{
	let content;
	Ok(InputOutputMacro {
		span: {
			let ident: Ident = input.parse()?;
			if ident != macro_ident {
				bail!(ident.span() => "Expected `{macro_ident}`")
			}
			ident.span()
		},
		not_token: input.parse()?,
		paren_token: syn::parenthesized!(content in input),
		content: content_parser(&content)?,
		semi_token: input.parse()?
	})
}

impl Parse for Input {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		parse(input, "input", Parse::parse).map(Into::into)
	}
}

impl Parse for Output {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		parse(input, "output", Punctuated::parse_separated_nonempty).map(Into::into)
	}
}
