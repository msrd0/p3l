use super::{Block, Expression};
use proc_macro2::{Ident, Span};
use syn::{
	parse::{Parse, ParseStream},
	token::{Brace, Paren},
	Token
};

#[derive(Clone, Debug)]
pub enum Condition {
	Bool(Expression),
	LetSome {
		let_token: Token![let],
		some_span: Span,
		paren_token: Paren,
		ident: Ident,
		eq_token: Token![=],
		expr: Expression
	}
}

impl Parse for Condition {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		Ok(if input.peek(Token![let]) {
			let content;
			Self::LetSome {
				let_token: input.parse()?,
				some_span: {
					let some: Ident = input.parse()?;
					if some != "Some" {
						bail!(some.span() => "Expected `Some`");
					}
					some.span()
				},
				paren_token: syn::parenthesized!(content in input),
				ident: content.parse()?,
				eq_token: input.parse()?,
				expr: input.parse()?
			}
		} else {
			Self::Bool(input.parse()?)
		})
	}
}

#[derive(Clone, Debug)]
pub enum ElseBranch {
	If(Box<If>),
	Block(Block)
}

impl Parse for ElseBranch {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		Ok(if input.peek(Token![if]) {
			Self::If(input.parse()?)
		} else if input.peek(Brace) {
			Self::Block(input.parse()?)
		} else {
			bail!(input.span() => "Expected `if` or block");
		})
	}
}

#[derive(Clone, Debug)]
pub struct If {
	pub if_token: Token![if],
	pub condition: Condition,
	pub then_branch: Block,
	pub else_branch: Option<(Token![else], ElseBranch)>
}

impl Parse for If {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		Ok(Self {
			if_token: input.parse()?,
			condition: input.parse()?,
			then_branch: input.parse()?,
			else_branch: input
				.peek(Token![else])
				.then(|| syn::Result::Ok((input.parse()?, input.parse()?)))
				.transpose()?
		})
	}
}
