use super::{Block, Generics, Type};
use proc_macro2::Ident;
use syn::{
	parse::{Parse, ParseStream},
	punctuated::Punctuated,
	token::Paren,
	Token
};

#[derive(Clone, Debug)]
pub struct FnArg {
	pub ident: Ident,
	pub colon_token: Token![:],
	pub ty: Type
}

impl Parse for FnArg {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		Ok(Self {
			ident: input.parse()?,
			colon_token: input.parse()?,
			ty: input.parse()?
		})
	}
}

#[derive(Clone, Debug)]
pub struct Fn {
	pub fn_token: Token![fn],
	pub ident: Ident,
	pub generics: Option<Generics<Ident>>,
	pub paren_token: Paren,
	pub inputs: Punctuated<FnArg, Token![,]>,
	pub output: Option<(Token![->], Type)>,
	pub block: Block
}

impl Parse for Fn {
	fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
		let content;
		Ok(Self {
			fn_token: input.parse()?,
			ident: input.parse()?,
			generics: input.peek(Token![<]).then(|| input.parse()).transpose()?,
			paren_token: syn::parenthesized!(content in input),
			inputs: Punctuated::parse_terminated(&content)?,
			output: input
				.peek(Token![->])
				.then(|| syn::Result::Ok((input.parse()?, input.parse()?)))
				.transpose()?,
			block: input.parse()?
		})
	}
}
