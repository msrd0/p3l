#![warn(rust_2018_idioms, rustdoc::broken_intra_doc_links)]
#![deny(elided_lifetimes_in_paths)]
#![forbid(unsafe_code)]

use anyhow::Context as _;
use bat::PrettyPrinter;
use clap::Parser;
use compiler::{
	compile::{compile, CompileResult},
	diagnostic::{Diagnostic, Source},
	set_verbose
};
use std::{
	fs::{self, File},
	io::{Cursor, Read as _, Write as _},
	path::PathBuf,
	process::{self, Command}
};
use tempfile::tempdir;

const STDLIB_ARCHIVE: &[u8] = include_bytes!(env!("ARCHIVE_PATH"));

const MANIFEST: &[u8] = br#"
[workspace]
members = [".", "stdlib"]

[package]
name = "program"
version = "0.0.0"
publish = false
edition = "2021"

[[bin]]
name = "program"
path = "main.rs"

[dependencies]
bpaf = "0.4.7"
indexmap = "1.9"
indicatif = { version = "0.17", features = ["rayon"] }
rayon = "1.5"
stdlib = { path = "stdlib" }

[profile.release]
debug = true
lto = true
"#;

#[derive(Parser)]
#[clap(about, author, version = concat!(env!("VERGEN_GIT_COMMIT_COUNT"), "-g", env!("VERGEN_GIT_SHA_SHORT")))]
struct Args {
	/// Enable verbose output.
	#[clap(short, long, action)]
	verbose: bool,

	/// Enable compilation in release mode.
	#[clap(long, action)]
	release: bool,

	/// The input file to compile.
	#[clap(value_parser)]
	input_file: PathBuf,

	/// The output binary.
	#[clap(short, value_parser, default_value = "a.out")]
	output_file: PathBuf
}

fn main() -> anyhow::Result<()> {
	// parse command line arguments
	let args = Args::parse();
	set_verbose(args.verbose);

	// create temporary directory to assemble all files
	let tempdir = tempdir().context("Failed to create temporary directory")?;
	let tmpdir = tempdir.path();

	// extract the stdlib archive into the tempdir
	let mut archive = tar::Archive::new(Cursor::new(STDLIB_ARCHIVE));
	archive
		.unpack(&tmpdir)
		.context("Failed to unpack the stdlib")?;

	// create the manifest
	let manifest_path = tmpdir.join("Cargo.toml");
	File::create(&manifest_path)
		.context("Failed to create the manifest")?
		.write_all(MANIFEST)
		.context("Failed to write the manifest")?;

	// create the main file from the transcribed program
	let mut input = String::new();
	File::open(&args.input_file)
		.context("Failed to open the input file")?
		.read_to_string(&mut input)
		.context("Failed to read the input file")?;
	let (main_rs, funs_rs, types_rs) = match compile(&input) {
		Ok(CompileResult::Ok {
			main_rs,
			funs_rs,
			types_rs
		}) => (main_rs, funs_rs, types_rs),
		Ok(CompileResult::InternalErr(code, err)) => {
			if args.verbose {
				PrettyPrinter::new()
					.input_from_bytes(code.as_bytes())
					.language("Rust")
					.line_numbers(true)
					.print()
					.unwrap();
			}
			Diagnostic::ice(err)
				.eprint(Source::new(&code))
				.context("Failed to print ICE")?;
			process::exit(1);
		},
		Err(err) => {
			let filename = args.input_file.file_name().unwrap().to_str().unwrap();
			err.eprint(Source::new(&input).with_filename(filename))
				.context("Failed to print error message")?;
			process::exit(1);
		}
	};
	let main_rs = prettyplease::unparse(&main_rs);
	let funs_rs = prettyplease::unparse(&funs_rs);
	let types_rs = prettyplease::unparse(&types_rs);
	if args.verbose {
		PrettyPrinter::new()
			.input(bat::Input::from_bytes(types_rs.as_bytes()).name("types.rs"))
			.input(bat::Input::from_bytes(funs_rs.as_bytes()).name("funs.rs"))
			.input(bat::Input::from_bytes(main_rs.as_bytes()).name("main.rs"))
			.language("Rust")
			.line_numbers(true)
			.print()
			.unwrap();
	}
	fs::write(tmpdir.join("main.rs"), main_rs).context("Failed to write main.rs file")?;
	fs::write(tmpdir.join("funs.rs"), funs_rs).context("Failed to write funs.rs file")?;
	fs::write(tmpdir.join("types.rs"), types_rs)
		.context("Failed to write types.rs file")?;

	// run cargo
	let target_dir = args
		.input_file
		.parent()
		.expect("How can a file not be in a directory?")
		.join("target")
		.join(
			args.input_file
				.file_name()
				.expect("How can a file not have a filename?")
		);
	fs::create_dir_all(&target_dir)?;
	let mut cmd = Command::new("cargo");
	cmd.arg("build")
		.arg("--target-dir")
		.arg(&target_dir)
		.arg("--manifest-path")
		.arg(&manifest_path);
	if args.release {
		cmd.arg("--release");
	}
	cmd.env("RUST_BACKTRACE", "0");
	let success = cmd
		.status()
		.context("Failed to execute cargo build")?
		.success();
	if !success {
		anyhow::bail!("cargo returned non-successful exit code");
	}
	fs::copy(
		target_dir
			.join(args.release.then_some("release").unwrap_or("debug"))
			.join("program"),
		&args.output_file
	)?;

	Ok(())
}
