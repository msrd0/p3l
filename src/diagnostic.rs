use ariadne::{Label, Report, ReportKind};
use proc_macro2::{LineColumn, Span};
use std::{
	fmt::{Debug, Display},
	io::{self, Write}
};

#[derive(Clone, Copy, Eq, PartialEq)]
pub struct Source<'a> {
	pub code: &'a str,
	pub filename: Option<&'a str>
}

impl<'a> Source<'a> {
	pub fn new(code: &'a str) -> Self {
		Self {
			code,
			filename: None
		}
	}

	pub fn with_filename(mut self, filename: &'a str) -> Self {
		self.filename = Some(filename);
		self
	}

	fn offset(&self, at: LineColumn) -> usize {
		let line_offset: usize = self
			.code
			.split('\n')
			.take(at.line - 1)
			.map(|line| line.len() + 1)
			.sum();
		line_offset + at.column
	}
}

pub struct SourceSpan<'a> {
	source: Source<'a>,
	start: usize,
	end: usize
}

impl<'a> ariadne::Span for SourceSpan<'a> {
	type SourceId = Source<'a>;

	fn source(&self) -> &Self::SourceId {
		&self.source
	}

	fn start(&self) -> usize {
		self.start
	}

	fn end(&self) -> usize {
		self.end
	}
}

struct SourceCache {
	source: ariadne::Source
}

impl SourceCache {
	fn new(source: Source<'_>) -> Self {
		Self {
			source: ariadne::Source::from(source.code)
		}
	}
}

impl ariadne::Cache<Source<'_>> for SourceCache {
	fn fetch(&mut self, _: &Source<'_>) -> Result<&ariadne::Source, Box<dyn Debug>> {
		Ok(&self.source)
	}

	fn display<'a>(&self, id: &'a Source<'_>) -> Option<Box<dyn Display + 'a>> {
		id.filename
			.map(|filename| Box::new(filename) as Box<dyn Display + 'a>)
	}
}

pub struct Diagnostic {
	span: Span,
	msg: String,
	labels: Vec<(Span, String)>,
	note: Option<String>,
	help: Option<String>
}

impl Diagnostic {
	pub fn new<T, M>(span: Span, topic: T, msg: M) -> Self
	where
		T: Into<String>,
		M: Into<String>
	{
		Self {
			span,
			msg: topic.into(),
			labels: vec![(span, msg.into())],
			note: None,
			help: None
		}
	}

	pub fn with_label<M>(mut self, span: Span, msg: M) -> Self
	where
		M: Into<String>
	{
		self.labels.push((span, msg.into()));
		self
	}

	pub fn with_note<N>(mut self, note: N) -> Self
	where
		N: Into<String>
	{
		self.note = Some(note.into());
		self
	}

	pub fn with_help<H>(mut self, help: H) -> Self
	where
		H: Into<String>
	{
		self.help = Some(help.into());
		self
	}

	pub fn no_such_field<M>(span: Span, msg: M) -> Self
	where
		M: Into<String>
	{
		Self::new(span, "No such field", msg)
	}

	pub fn type_mismatch<T, U>(
		expected_ty: T,
		expected_span: Span,
		expression_ty: U,
		expression_span: Span
	) -> Self
	where
		T: Display,
		U: Display
	{
		Self {
			span: expression_span,
			msg: "Type Mismatch".into(),
			labels: if expected_span.start() < expression_span.start() {
				vec![
					(expected_span, format!("Expected type `{expected_ty}`,")),
					(
						expression_span,
						format!("but expression is of type `{expression_ty}`")
					),
				]
			} else {
				vec![
					(
						expression_span,
						format!("Expression is of type `{expression_ty}`,")
					),
					(expected_span, format!("but expected type `{expected_ty}`")),
				]
			},
			note: None,
			help: None
		}
	}

	pub fn ice(err: syn::Error) -> Self {
		Self::new(err.span(), "Internal Compiler Error", err.to_string()).with_note(
			"The compiler produced invalid intermediate Rust code. This is a bug."
		)
	}
}

impl From<syn::Error> for Diagnostic {
	fn from(err: syn::Error) -> Self {
		Self::new(err.span(), "Syntax Error", err.to_string())
	}
}

impl Diagnostic {
	fn into_report(self, source: Source<'_>) -> Report<SourceSpan<'_>> {
		let start_offset = source.offset(self.span.start());
		let mut report =
			Report::build(ReportKind::Error, source, start_offset).with_message(self.msg);
		for (label_span, label_msg) in self.labels {
			report.add_label(
				Label::new(SourceSpan {
					source,
					start: source.offset(label_span.start()),
					end: source.offset(label_span.end())
				})
				.with_message(label_msg)
			);
		}
		if let Some(note) = self.note {
			report.set_note(note);
		}
		if let Some(help) = self.help {
			report.set_help(help);
		}

		report.finish()
	}

	pub fn write<W>(self, source: Source<'_>, w: W) -> io::Result<()>
	where
		W: Write
	{
		self.into_report(source).write(SourceCache::new(source), w)
	}

	pub fn eprint(self, source: Source<'_>) -> io::Result<()> {
		self.into_report(source).eprint(SourceCache::new(source))
	}
}
